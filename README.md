# Django Canary Reports

## Overview
A Django package to create reports using models or QuerySets.

Most report writing comprises filters, sorts, and pagination. Many developers write this glue code over and over again, with slight variations specific to the implementation.

My goal is to create a package that will allow a developer to create a report with a mix of filters and sorts very quickly, but with the ability to extend the package and add any custom functionality that might be needed.

Hopefully, this would actually turn into a collection of filters, sorts, views, etc., that can be shared by the community or possibly included with the package itself.

## UPDATE (August 4, 2012)!!!
I still want to add magic in a few places, but I've got things in a really good state.  My next step will be to fill out the documentation on customzing views, filters, sorts, aggregates, editables , etc.  I've added so much lately that I should list some of the cool features:

* Multi-sort columns (meaning you can sort by one column first, then another, and so on)
* Date range filter, date empty/exists filter, column search, foreign key filter, choice filter.
* Inline editable columns with different widgets for different column types (using the very awesome inplaceeditform project).
* Aggregates: you can add any Django aggregate to a column (sum, avg, count, min, max).
* Handy display widgets for formatted dates, links, admin links, email links and more.
* All views, filters, sorts, aggregates and editables are customizable.

Next things:

* Extend documentation!
* Expose "rows per page" control to user.
* Visual documentation of view -> column -> filter <- sort -> flow and how to customize things.
* Settle on class/method/argument structure and clearly document all options.
* Dance!  Er, report!

## Read the Docs!!!

I've spent quite a bit of time designing and documenting the project.  Please read the documentation and give me feedback on how it works for you.

[Read the Docs!!!](http://django-canary.readthedocs.org/en/latest/)
