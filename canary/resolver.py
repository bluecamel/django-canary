from django.db import models


class Resolver(object):
    """
    A class to handle the magic of finding attributes given as
    arguments to columns and filters.

    To use, simply instantiate the class, handing over all
    objects on which you want to search for the attribute, in
    order of preference.  Then call resolve, passing the attribute
    and (optionally) a default value to return if the attribute was
    not found.

    Example:
        value = Resolver(obj, view).resolve('get_absolute_url', '')
    """
    def __init__(self, *objects):
        """
        Instantiate by passing all objects you want to look look
        for an attribute on, in order of preference.

        Args:
            objects: An iterable of objects.
        """
        self.objects = objects
        if not self.objects:
            self.objects = []

    def resolve(self, attr, default=None):
        """
        Resolve the given attribute from the available objects.

        Args:
            attr: The attribute that you want to find.
            default: A default value, to be used if the attribute isn't found.

        Returns:
            The resolved attribute (or results of calling a method) or default.
        """
        if isinstance(attr, basestring):
            return self.resolve_string(attr, default)

        elif isinstance(attr, dict):
            return self.resolve_dict(attr, default)

        elif isinstance(attr, list):
            return self.resolve_list(attr, default)

        elif isinstance(attr, tuple):
            return self.resolve_tuple(attr, default)

    def resolve_callable(self, attr):
        """
        Before returning a found attribute, check if it is
        a method, and handle appropriately.

        Args:
            attr: The found attribute.

        Returns:
            If the attribute is a method, return the results of calling
            the method.

            If the attribute is a view method, return the results of calling
            the method (passing the QuerySet object).  Note: This relies on the
            QuerySet having been provided to the class upon instantiation.
        """
        if callable(attr):
            from views import BasicReport

            im_class = getattr(attr, 'im_class', None)

            # If the attribute is a method on the view, we need
            # to hand it the current object from the QuerySet.
            if im_class and isinstance(im_class(), BasicReport):
                for obj in self.objects:
                    if isinstance(obj, models.Model):
                        return attr(obj)
                        break

            # The attribute is just a method.  Return the
            # results of calling it.
            return attr()

        # The attribute is not a method.  Return it as it is.
        return attr

    def resolve_list(self, attr, default):
        """
        Resolve a list.

        Args:
            attr: The list to be resolved.

        Returns:
            A copy of the list, with all values resolved.
        """
        new_list = []
        for item in attr:
            new_list.append(self.resolve(item))
        return new_list

    def resolve_dict(self, attr, default):
        """
        Resolve a dictionary.

        Args:
            attr: The dictionary to be resolved.

        Returns:
            A copy of the dictionary, with all values resolved.
        """
        new_dict = {}
        for k, v in attr.iteritems():
            new_dict[self.resolve(k, default)] = self.resolve(v, default)
        return new_dict

    def resolve_string(self, attr, default=None):
        """
        Resolve a string.

        Args:
            attr: A string believed to be an attribute on one of the available
                  objects.
            default: A value to return if the attribute can't be resolved.

        Returns:
            The attribute value or the results of calling an attribute that
            is a method.

            If those fail, return default or None.
        """
        if attr.find('__') > 0:  # object "path"
            path = attr.split('__')
            if len(path) > 0:
                for obj in self.objects:
                    try:  # try the next attribute in the path
                        next_obj = self.resolve_callable(
                            getattr(obj, path[0]))

                        objects = [next_obj]
                        for obj in self.objects:
                            objects.append(obj)

                        return Resolver(next_obj, *objects).resolve(
                            '__'.join(path[1:]), default)

                    except:
                        pass

        else:
            for obj in self.objects:
                try:  # look for field on the object
                    obj_attr = getattr(obj, attr)

                except:
                    pass

                else:
                    from columns import Column
                    from views import BasicReport

                    # As long as this is not a column on a view,
                    # return the found attribute.
                    if not (isinstance(obj, BasicReport) and \
                        isinstance(obj_attr, Column)):
                        return self.resolve_callable(obj_attr)

            # TODO: Still need this??
            #if hasattr(attr, 'render'):
            #    # If this is a column, return the results of
            #    # rendering the column.
            #    # First, we need the view and QuerySet obj.
            #    from views import BasicReport

            #    view = None
            #    obj = None

            #    for _obj in self.objects:
            #        if isinstance(_obj, BasicReport):
            #            view = _obj

            #        elif isinstance(_obj, models.Model):
            #            obj = _obj

            #    if view and obj:
            #        pass
            #        #return attr.render(view, obj)

        # The attribute was not found.  Return the default or None.
        if default:
            return default
        return None

    def resolve_tuple(self, attr, default):
        """
        Resolve a tuple.

        Args:
            attr: The tuple to be resolved.

        Returns:
            A copy of the tuple, with all values resolved.
        """
        new_list = []
        for item in attr:
            new_list.append(self.resolve(item, default))
        return tuple(new_list)
