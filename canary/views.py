from collections import OrderedDict

from django.contrib.admin.views.decorators import staff_member_required
from django.core.paginator import Paginator
from django.db.models.loading import get_model
from django.db.models.query import QuerySet
from django.http import HttpResponse
from django.template import RequestContext
from django.utils import simplejson
from django.views.generic import TemplateView
from django.views.generic.edit import FormMixin, ProcessFormView, UpdateView

from columns import Column, ColumnHeader, get_column_for_field
from forms import ColumnEditForm, PaginationForm
from resolver import Resolver
from sorts import SortManager


class ReportBase(type):
    """
    Metaclass used for creating a view.

    The major reason for this is to collect the columns in the order they were
    defined on the class
    """

    def __new__(cls, name, bases, attrs):
        """
        Create the new class.

        Args:
            name: The class name.
            bases: Base classes.
            attrs: The classe attributes.

        Returns:
            The new report view class.
        """
        # Find all attributes that extend the Column class and save them to a
        # list in order of definition.
        ordered_columns = []
        for attr_name in attrs.keys():
            attr_value = attrs.get(attr_name, None)
            if isinstance(attr_value, Column):
                ordered_columns.append((attr_name, attr_value))
                attrs.pop(attr_name)

        # Sort the columns by creation order.
        ordered_columns.sort(key=lambda item: item[1].creation_order)

        # Build the columns OrderedDict.
        columns = OrderedDict()
        for column_name, column in ordered_columns:
            column.name = column_name
            columns[column_name] = column

        # Create the new class.
        new_cls = type.__new__(cls, name, bases, attrs)

        # Create _meta object.
        new_cls._meta = type('meta', (object,), {'columns': columns})

        return new_cls


class BasicReport(TemplateView):
    """
    Very basic base class that inlcudes the metaclass and sets the default
    template_name.
    """
    __metaclass__ = ReportBase
    template_name = 'admin/canary/report.html'

    # TODO: ease customization of JS and CSS.
    extra_css = None
    extra_js = None
    js_context = None

    def __init__(self):
        """
        Create the BasicReport.

        Sets:
            extra_css: Additional CSS to include in the view.
            extra_js: Additional javascript to include in the view.
            js_context: A dictionary of JSON values to include before the extra_js, such as:
                {
                    'editColumnURL': '/canary/edit_column/'
                }
        """
        self.extra_css = getattr(self.Meta, 'extra_css', {})
        self.extra_js = getattr(self.Meta, 'extra_js', {})
        self.js_context = getattr(self.Meta, 'js_context', {})

    def add_extra_media(self, extra_css, extra_js, js_context):
        """
        Add additional media to the view.

        Args:
            extra_css: A dictionary with CSS info, such as:
                {
                    'css/extra_styles.css': {'media': 'screen, projection'},
                    'css/more_styles.css': None
                }

            extra_js: An iterable of iterables with JS info, such as:
                {
                    'js/extra_scripty_magics.js': None,
                    'js/more_scripters.js': None
                }

            js_context: A dictionary of JSON strings, such as:
                {
                    'editableURL': '/canary/edit_column/',
                }

                The extra data you want to include is then available to your
                included JS scripts, from canary.editableURL.

        Sets:
            extra_css: Adds to existing extra_css.
            extra_js: Adds to existing extra_js.
            js_context: Adds to existing js_context.
        """
        if extra_css:
            self.extra_css.update(extra_css)

        if extra_js:
            self.extra_js.update(extra_js)

        if js_context:
            self.js_context.update(js_context)

    def get_extra_css(self):
        """
        Return the dictionary with default parameters set.

        Returns:
            A dictionary, such as:
                 {
                     'css/extra_styles.css': {'media': 'screen, projection', 'type': 'text/css', 'rel': 'stylesheet'},
                     'css/more_styles.css': {'type': 'text/css', 'rel': 'stylesheet'},
                 }
        """
        for url, params in self.extra_css.items():
            if 'type' not in params.keys():
                params['type'] = 'text/css'

            if 'rel' not in params.keys():
                params['rel'] = 'stylesheet'

            #self.extra_css[url] = params  # TODO: huhface??

        return self.extra_css

    def get_extra_js(self):
        """
        Return the dictionary with default parameters set.

        Returns:
            A dictionary, such as:
                 {
                     'js/extra_scripty_magics.js': {'type': 'text/javascript'},
                     'js/more_scripters.js': {'type': 'text/javascript'}
                 }
        """
        for url, params in self.extra_js.items():
            if 'type' not in params.keys():
                params['type'] = 'text/javascript'

            #self.extra_js[url] = params

        return self.extra_js


class QueryReport(BasicReport):
    """
        Build a report using a provided queryset, along with column definitions.
    """

    def __init__(self, *args, **kwargs):
        """
        Create the QueryReport.

        Sets:
            _meta.queryset: The Django QuerySet.
            _meta.breadcrumbs: A list of tuples, such as:
                [
                    ('App Name', '/admin/app_name/'),
                    ('Reports', '/admin/app_name/reports/'),
                    ('Report Name', '/admin/reports/report_name/')
                ]
        """
        BasicReport.__init__(self, *args, **kwargs)

        self.getattr_warn = False
        self.header = None

        self._meta.queryset = getattr(self.Meta, 'queryset', None)
        self._meta.breadcrumbs = getattr(self.Meta, 'breadcrumbs', [])

    def filter_queryset(self, request):
        """
            Your second chance to affect the queryset, with filters.

            You can override this method to change the way filters are handled,
            or even just to add a final filter onto your query.
        """
        for column_name, column in self._meta.columns.iteritems():
            for column_filter in column.filters:
                self._meta.queryset = column_filter.filter_queryset(request,
                    self._meta.queryset)

    def get_app_label(self):
        """
        Return the app label from the queryset.
        """
        return self._meta.queryset.model._meta.app_label

    def get_model_name(self):
        """
        Return the model name from the queryset.
        """
        return self._meta.queryset.model._meta.module_name

    def get(self, request, *args, **kwargs):
        """
        Handle a GET request.
        """
        return self.get_post(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """
        Handle a POST request.
        """
        return self.get_post(request, *args, **kwargs)

    def get_post(self, request, *args, **kwargs):
        """
        Handle any request.
        """
        self.request = request
        self.load_queryset(request)
        self.load_filters(request)
        self.load_sort_manager(request)
        self.filter_queryset(request)
        self.load_aggregates(request)
        self.sort_queryset(request)
        self.limit_queryset(request)
        # TODO: cache querysets for performance.
        #self.cache_queryset(request)
        self.load_headers(request)

        return self.render_to_response({'report': self, 'report_meta': self._meta})

    def get_row(self, obj):
        """
        Given the current object from the QuerySet, return a list for the row.
        """
        row = []
        for column_name, column in self._meta.columns.items():
            row.append(column.render(self, obj))
        return row

    def get_rows(self):
        """
        Get a list of row lists from the QuerySet.
        """
        return [row for row in self.iter_rows()]

    def iter_rows(self):
        """
        Iterate over the QuerySet, returning a row list on each iteration.
        """
        #for obj in self._meta.queryset:
        for obj in self.page.object_list:
            yield self.get_row(obj)

    def limit_queryset(self, request):
        """
        Limit/paginate the QuerySet.

        Args:
            request: The Django request object.

        Sets:
            _meta.queryset: The Django QuerySet.
        """
        if request.method == 'POST':
            form = PaginationForm(request.POST)
            if form.is_valid():
                page = form.cleaned_data['page']
                rows_per_page = form.cleaned_data['rows_per_page']

        else:
            form = PaginationForm()
            page = 1
            rows_per_page = 25  # TODO: shared constant with form

        # TODO: namespace !!!
        self.paginator = Paginator(self._meta.queryset, rows_per_page)

        try:
            # TODO: namespace !!!
            self.page = self.paginator.page(page)

        except:
            # TODO: namespace !!!
            self.page = self.paginator.page(1)

        # TODO: namespace !!!
        self.pagination_form = form

    def load_aggregates(self, request):
        """
        Load all aggregates from all columns.

        Args:
            request: The Django request object.

        Sets:
            aggregates: A dictionary of aggregates.
        """
        self.aggregates = {}

        for column_name, column in self._meta.columns.iteritems():
            for column_aggregate in column.aggregates:
                column_aggregate.column = column
                column_aggregate.load(request, self._meta.queryset)
            self.aggregates[column_name] = column.aggregates

    def load_filters(self, request):
        """
        Load all filters from all columns.

        Args:
            request: The Django request object.

        Sets:
            filters: A dictionary of filters.
            order_by: A list of columns, in order of sort preference.
        """
        self.filters = {}
        self.order_by = []  # TODO: remove all references to this.  # TODO: remove all references to that.  # TODO: oh, teh noes!

        for column_name, column in self._meta.columns.iteritems():
            self.add_extra_media(column.extra_css, column.extra_js, column.js_context)

            if column.filters:
                for column_filter in column.filters:
                    column_filter.column = column
                    # Load data from the request for the filter.
                    # TODO: Handle more of this in the view so that filters
                    # don't have to worry about request.method.
                    column_filter.load(request, self._meta.queryset)
                    self.add_extra_media(column_filter.extra_css, column_filter.extra_js, column_filter.extra_js)
                self.filters[column_name] = column.filters

    def load_headers(self, request):
        """
        Load all headers.  Headers include filters and sorts, so those must
        already be loaded.

        Args:
            request: The Django request object.

        Sets:
            headers: A list of ColumnHeader objects.
        """
        self.headers = []
        for column_name, column in self._meta.columns.items():
            header = ColumnHeader(column, self)
            self.headers.append(header.render(request))

    def load_queryset(self, request):
        """
        Your first chance to affect the queryset.

        Args:
            request: The Django request object.

        Sets:
            queryset: A Django QuerySet.

        Raises:
            AttributeError: QueryReport.Meta.queryset isn't defined.
        """
        if not isinstance(self._meta.queryset, QuerySet):
            raise AttributeError('You must define a QueryReport.Meta.queryset method.')

    def load_sort_manager(self, request):
        """
        Create the SortManager.

        Args:
            request: The Django request.

        Sets:
            _meta.sort_manager: SortManager instance.
        """
        self.sort_manager = SortManager(self, request, self._meta.columns)

    def resolve_text(self, obj, attr, default=None):
        """
        Resolve the given attribute from the given object or the current view.

        Args:
            obj: The object from the QuerySet.
            attr: The attribute to resolve.
            default: The value to return if the attribute can't be found.

        Returns:
            The resolved value or default.
        """
        return Resolver(obj, self).resolve(attr, default)

    def sort_queryset(self, request):
        """
        Your last chance to affect the queryset.

        Expected to apply an order_by to the QuerySet.

        TODO: implement multi-column sort.

        Args:
            request: The Django request object.

        Sets:
            _meta.queryset: A Django QuerySet.
        """
        self._meta.queryset = self.sort_manager.sort_queryset(self._meta.queryset)


class ModelReport(QueryReport):
    """
    An extension of QueryReport that builds a report from a given model.
    """

    def __init__(self, *args, **kwargs):
        QueryReport.__init__(self, *args, **kwargs)

        # Make sure the model is defined.
        self._meta.model = getattr(self.Meta, 'model', None)
        if not self._meta.model:
            raise Exception(u'You must provide a model.')

        # Load columns from the model.
        self.load_columns()

    def load_columns(self):
        """
        Load columns from the model and create canary columns for each.

        Sets:
            _meta.columns: An OrderedDict of column.
        """
        columns = OrderedDict()
        for field in self._meta.model._meta.fields:
            column = get_column_for_field(field)
            column.name = field.name
            columns[field.name] = column

        self._meta.columns = columns

    def load_queryset(self, request):
        """
        Load the QuerySet.  First try to load from a given QuerySet
        (defined as QuerySet.Meta.queryset).  If none was provided, load a
        QuerySet of all objects for the model.

        Args:
            request: The Django request object.

        Sets:
            _meta.queryset: A Django QuerySet.
        """
        default_queryset = False

        try:
            QueryReport.load_queryset(self, request)

        except:
            default_queryset = True

        if default_queryset and self._meta.queryset:
            queryset = self._meta.queryset

        else:
            queryset = self._meta.model.objects.all()

        self._meta.queryset = queryset


class ColumnEdit(FormMixin, ProcessFormView):
    form_class = ColumnEditForm

    def form_valid(self, form):
        response_data = {'success': False}

        attr_id = form.cleaned_data['id']
        attr_value = form.cleaned_data['value']

        attr_id_parts = attr_id.split('--')

        if len(attr_id_parts) == 5:
            prefix, app_label, model_name, field_name, obj_pk = attr_id_parts
            model_class = get_model(app_label, model_name)

            for field in model_class._meta.fields:
                if field.name == field_name:
                    try:
                        model_object = model_class._default_manager.get(pk=obj_pk)

                    except:
                        pass

                    else:
                        perm = '{app_label}.change_{model_name}'.format(
                            app_label=app_label, model_name=model_name)

                        if self.request.user.has_perm(perm):
                            setattr(model_object, field_name, attr_value)
                            model_object.save()

                            response_data['value'] = attr_value
                            response_data['success'] = True

                    break

        return HttpResponse(simplejson.dumps(response_data), mimetype='application/json')

    def form_invalid(self, form):
        response_data = {'success': False}
        return HttpResponse(simplejson.dumps(response_data), mimetype='application/json')


column_edit = staff_member_required(ColumnEdit.as_view())
