#from warnings import warn

from django.core.urlresolvers import reverse
from django.template import Context, Template
from django.template.loader import get_template

import filters as canary_filters
import sorts as canary_sorts


class Column(object):
    """
    Base column class that contains all shared functionality.
    """
    name = None  # This is set by the view.
    creation_order = 0
    extra_css = None
    extra_js = None
    js_context = None

    def __init__(self, label=None, filters=None, sorts=None, aggregates=None):
        """
        Create a Column.

        Args:
            label: The label used for column display (e.g. the header).
            filters: A list of filters to apply to the column.
        """
        # Save the order in which columns were defined.
        self.creation_order = Column.creation_order
        Column.creation_order += 1

        self.label = label

        self.filters = filters
        if not self.filters:
            self.filters = []

        self.sorts = sorts
        if not self.sorts:
            if self.sorts == False:
                self.sorts = ()
            else:
                self.sorts = [canary_sorts.ColumnSort()]

        self.aggregates = aggregates
        if not self.aggregates:
            self.aggregates = []

    def get_label(self):
        """
        Get the label (used to identify the column header, etc.).

        Returns:
            The label string.
        """
        label = self.name
        if self.label:
            label = self.label
        return label.replace('_', ' ').title()

    def render(self, view, obj):
        """
        Render the column.
        """
        raise NotImplementedError('You must define a render method.')


class Text(Column):
    """
    The most basic column that most people will use.

    Extends canary.columns.Column.
    """
    resolve = ('text', 'name')
    name = None

    def __init__(self, text=None, default=None, *args, **kwargs):
        """
        Create a Text column.

        Unless overridden, the column is sortable and the Search filter is
        included by default.

        Args:
            text: The column or attribute name to use for getting the text to display.
            default: A default value to display if the text attribute can't be resolved.

        Example:
            thinger = Text('contact__name', None, 'Thinger', (SearchFilter,))
        """
        # If no filters are provided, supply Search filter by default.
        column_filters = kwargs.pop('filters', None)
        if column_filters == None:
            column_filters = [canary_filters.Search()]
        kwargs['filters'] = column_filters

        Column.__init__(self, *args, **kwargs)

        self.text = text
        self.default = default

        self.resolve += ('text', 'name')

    def get_field_name(self):
        """
        Used by filters in order to filter by a Django column name.  It is used
        to build the kwargs passed to QuerySet.filter.

        See the filter_queryset methods on included filters to see how this
        is used.
        """
        if self.text:
            return self.text
        return self.name

    def get_text(self, resolved_text):
        """
        Get the text value of the column.

        Args:
            resolved_text: A dictionary of resolved values.

        Returns:
            The value of the column.
        """
        field_name = 'text'
        if not self.text:
            field_name = 'name'

        return resolved_text[field_name]

    def resolve_text(self, view, obj, default=None):
        """
        Resolve all attributes of the column that are defined in Column.resolve.

        Args:
            view: The report view.
            obj: The QuerySet object.
            default: A default value to use if the text can't be resolved.

        Returns:
            A dictionary where the attribute names from Column.resolve are the
            keys.  The values are the resolved values.
        """
        resolved_text = {}
        for attr_name in self.resolve:
            attr_value = getattr(self, attr_name)
            resolved_text[attr_name] = view.resolve_text(obj, attr_value, default)

        # If the text resolved to the field name, then set the text to an
        # empy string.
        if resolved_text['text'] == self.get_field_name():
            resolved_text['text'] = ''

        return resolved_text

    def render(self, view, obj):
        """
        Render the column value for an object in the QuerySet.

        Args:
            view: The report view.
            obj: The QuerySet object.

        Returns:
            A string containing the resolved text.
        """
        resolved_text = self.resolve_text(view, obj)
        return self.get_text(resolved_text)


class Tag(Text):
    """
    The next most basic Column, this is the base of all columns that should
    be displayed as HTML elements.

    Extends canary.columns.Text.
    """
    tag = None
    inline = False
    template_name = 'canary/columns/tag.html'

    def __init__(self, text=None, autoid=None, autoid_prefix=None, params=None, allow_empty=False, *args, **kwargs):
        """
        Create a Tag column.

        Args:
            text: The column name to use for getting the text to display.
            autoid: The attribute to use for the element id.
            autoid_prefix: The prefix to use for the element id.
            params: A dictionary of parameters to include in the element declaration.
            allow_empty: A boolean indicating whether the element should be rendered even if there is not content.

        Example:
            a = Tag('title__name', 'title__id', 'report-link', ('report-link',), {'href': ''}, False, True, None, 'Thinger', (SearchFilter,))
        """
        if not self.tag:
            raise AttributeError('You must define a tag.')

        Text.__init__(self, text, *args, **kwargs)

        # TODO: This is a bit voodoo
        self.resolve += ('autoid', 'params')

        self.autoid = autoid

        self.autoid_prefix = autoid_prefix
        if not self.autoid_prefix:
            self.autoid_prefix = 'canary-column'

        self.params = params
        if not self.params:
            self.params = {}

        self.allow_empty = allow_empty

    def get_render_context(self, view, obj):
        """
        Return a dictionary of values to be included in the template context.

        Args:
            request: The Django request.
            view: The canary view.
            obj: The QuerySet object.

        Returns:
             A dictionary.
        """
        context = {
            'tag': self.tag,
            'inline': self.inline
        }

        resolved_text = self.resolve_text(view, obj, self.default)

        # Set the auto id.
        if resolved_text['autoid']:
            obj_id = resolved_text['autoid']
        else:
            obj_id = obj.pk

        autoid = '{prefix}--{app_label}--{model_name}--{column_name}--{obj_id}'.format(
            prefix=self.autoid_prefix, app_label=view.get_app_label(),
            model_name=view.get_model_name(), column_name=self.get_field_name(),
            obj_id=obj_id)

        resolved_text['params']['id'] = autoid

        context['params'] = resolved_text['params']

        # Build text string
        context['text'] = resolved_text['text']
        context['allow_empty'] = self.allow_empty

        return context

    def render(self, view, obj):  # TODO: note design difference between this and filters.
        """
        Renders the filter controls (form, buttons).

        Args:
            view: The canary view.
            obj: The QuerySet object.

        Returns:
            A string of HTML containing the filter controls.  By default,
            this is rendered as part of canary.columns.ColumnHeader.render.
        """
        template = get_template(self.template_name)
        context = self.get_render_context(view, obj)
        return template.render(Context(context))

    def resolve_text(self, view, obj, default=None):
        resolved_text = Text.resolve_text(self, view, obj, default)
        resolved_text['text'] = self.get_text(resolved_text)
        return resolved_text


class Link(Tag):
    """
    A link column.  Displays the value as an anchor tag.

    Extends canary.columns.Tag.
    """
    tag = 'a'

    def __init__(self, url=None, *args, **kwargs):
        """
        Create a Link column.

        Args:
            url: The attribute to reolve to get the url for a row object.

        Example:
            thinger = Link('title__get_absolute_url', 'title__name', 'title__id', 'report-link-', ('report-link',), {'rel': 'title__id'}, False, True, None, 'Thinger', (SearchFilter,))
        """
        Tag.__init__(self, *args, **kwargs)

        # TODO: This is a bit voodoo
        self.resolve += ('url',)

        self.url = url

    def resolve_text(self, view, obj, default=None):
        """
        Overriding to inject the 'href' attribute into the dictionary of
        resolved values.

        Args:
            view: The report view.
            obj: The QuerySet object.
            default: A default value to use if the text can't be resolved.

        Returns:
            A dictionary where the attribute names from Column.resolve are the
            keys.  The values are the resolved values.
        """
        resolved_text = Tag.resolve_text(self, view, obj, default)

        if not resolved_text['text']:
            resolved_text['text'] = resolved_text['url']

        if not resolved_text['url']:
            resolved_text['url'] = resolved_text['text']

        resolved_text['params']['href'] = resolved_text['url']

        return resolved_text


class EmailLink(Link):
    """
    A column that display an e-mail link.

    Extends canary.columns.Link.
    """
    def __init__(self, mailto=None, *args, **kwargs):
        """
        Create an EmailLink column.

        Args:
            mailto: The attribute to resolve to use for the e-mail address.
        """
        Link.__init__(self, '', *args, **kwargs)

        self.resolve += ('mailto',)

        self.mailto = mailto

    def resolve_text(self, view, obj, default=None):
        """
        Overriding to inject the 'mailto' and 'url' attributes into the
        dictionary of resolved values.

        Args:
            view: The report view.
            obj: The QuerySet object.

        Returns:
            A dictionary where the attribute names from Column.resolve are the
            keys.  The values are the resolved values.
        """
        # TODO: should this be moved to get_render_context??
        resolved_text = Link.resolve_text(self, view, obj, default=None)

        if not resolved_text['mailto']:  # text is assumed to be the email address
            resolved_text['mailto'] = self.get_text(resolved_text)

        if resolved_text['mailto']:
            resolved_text['params']['href'] = u'mailto:' + resolved_text['mailto']

        return resolved_text


class ReverseLink(Link):
    """
    A link column, where the URL is built using Django's reverse.

    Extends canary.columns.Link.
    """
    def __init__(self, text, reverse_path, reverse_args=None, reverse_kwargs=None, *args, **kwargs):
        """
        Create a ReverseLink column.

        The reverse_path, reverse_args and reverse_kwargs arguments are the same as you would use when reversing urls
        with Django's reverse utility (found in django.core.urlresolvers).

        The big difference is that you can provide attributes that will be resolved from the view or QuerySet object
        in order to provide (for example) the id or slug of the object being displayed.

        Args:
            text: The attribute to resolve to use for getting the text to display.
            reverse_path: The path to be passed to Django's reverse method.

        Example:
            thinger = ReverseLink('title__name', 'titles_title_change', ('title__id',), {}, 'title__id', 'report-admin-link-', ('report-link',), {'rel': 'title__id'}, False, True, None, 'Thinger', (SearchFilter,))

        In the example, the URL is built in a way similar to:
            title_id = obj.title.id
            url = reverse('titles_title_change', (title_id, ), {})
        """
        Link.__init__(self, text, '', *args, **kwargs)

        # TODO: This is a little voodoo
        self.resolve += ('reverse_args', 'reverse_kwargs')

        self.reverse_path = reverse_path

        self.reverse_args = reverse_args
        if not self.reverse_args:
            self.reverse_args = ()

        self.reverse_kwargs = reverse_kwargs
        if not self.reverse_kwargs:
            self.reverse_kwargs = {}

    def resolve_text(self, view, obj, default=None):
        """
        Overriding to inject the 'href' attribute into the dictionary of
        resolved values.

        Args:
            view: The report view.
            obj: The QuerySet object.
            default: A default value to use if the text can't be resolved.

        Returns:
            A dictionary where the attribute names from Column.resolve are the
            keys.  The values are the resolved values.
        """
        resolved_text = Link.resolve_text(self, view, obj, default)

        reverse_args = resolved_text['reverse_args']
        reverse_kwargs = resolved_text['reverse_kwargs']

        reverse_values = False

        if reverse_args:
            for arg in reverse_args:
                if arg:
                    reverse_values = True
                    break

        elif reverse_kwargs:
            for key, value in reverse_kwargs.iteritems():
                if key and value:
                    reverse_values = True
                    break

        if reverse_values:
            resolved_text['params']['href'] = reverse(self.reverse_path, args=resolved_text['reverse_args'], kwargs=resolved_text['reverse_kwargs'])

        return resolved_text


class AdminLink(ReverseLink):
    """
    A link column, where the URL is built using Django's reverse for an admin URL.

    Extends canary.columns.ReverseLink.
    """
    def __init__(self, text, reverse_path, reverse_args=None, reverse_kwargs=None, *args, **kwargs):
        """
        Create an AdminLink column.

        The reverse_path, reverse_args and reverse_kwargs arguments are the same as you would use when reversing urls
        with Django's reverse utility (found in django.core.urlresolvers), only the class prefixes the path with
        'admin:' for you, as a convenience.

        The big difference is that you can provide attributes that will be resolved from the view or QuerySet object
        in order to provide (for example) the id or slug of the object being displayed.

        Args:
            text: The attribute to resolve to use for getting the text to display.
            reverse_path: The path to be passed to Django's reverse method (prefixed by 'admin:').

        Example:
            thinger = AdminLink('title__name', 'titles_title_change', ('title__id',), {}, 'title__id', 'report-admin-link-', ('report-link',), {'rel': 'title__id'}, False, True, None, 'Thinger', (SearchFilter,))

        In the example, the URL is built in a way similar to:
            title_id = obj.title.id
            url = reverse('admin:titles_title_change', (title_id, ), {})
        """
        ReverseLink.__init__(self, text, 'admin:' + reverse_path, reverse_args, reverse_kwargs, *args, **kwargs)


class BooleanValue(Tag):
    """
    A column that gets its value from a Django models.BooleanField.

    Extends canary.columns.Tag.
    """
    tag = 'img'
    inline = True

    def __init__(self, *args, **kwargs):
        """
        Create a BooleanValue column.

        Example:
            thinger = BooleanValue(label='Thinger', (SearchFilter,))
        """
        Tag.__init__(self, *args, **kwargs)

    def resolve_text(self, view, obj, default=None):
        """
        Overriding to inject the 'text' attribute into the dictionary of
        resolved values.

        Args:
            view: The report view.
            obj: The QuerySet object.

        Returns:
            A dictionary where the attribute names from Column.resolve are the
            keys.  The values are the resolved values.
        """
        resolved_text = Tag.resolve_text(self, view, obj, default)

        # TODO: call this get_value??
        bool_value = self.get_text(resolved_text)

        if bool_value == True:
            src_url = 'img/admin/icon-yes.gif'
        else:
            src_url = 'img/admin/icon-no.gif'

        src_template = Template(
            '{% load adminmedia %}{% admin_media_prefix %}' + src_url)
        resolved_text['params']['src'] = src_template.render(Context({}))
        print 'rt', resolved_text

        return resolved_text


class Date(Text):
    """
    A column that gets its value from a Django models.DateField.

    Extends canary.columns.Text.
    """
    def __init__(self, text=None, default=None, strftime=None, *args, **kwargs):
        """
        Create a Date column.

        Unless overridden, the column is sortable and the DateRange filter is
        included by default.

        Args:
            text: The column name to use for getting the text to display.
            default: A default value to display if the text attribute can't be resolved.
            strftime: A string for date formatting.

        Sets:
            strftime: A string for date formatting.

        Example:
            thinger = Text('contact__name', None, '%Y/%m/%d', 'Thinger', None, (SearchFilter,))
        """
        column_filters = kwargs.pop('filters', None)
        if column_filters == None:
            column_filters = [canary_filters.DateRange(), canary_filters.DateEmpty()]
        kwargs['filters'] = column_filters

        Text.__init__(self, text, default, *args, **kwargs)

        self.strftime = strftime

    def render(self, view, obj):
        """
        Render the column value for an object in the QuerySet.

        Args:
            view: The report view.
            obj: The QuerySet object.

        Returns:
            A string containing the resolved text.
        """
        field_name = 'text'
        if not self.text:
            field_name = 'name'

        resolve = [field_name]
        for key in self.resolve:
            resolve.append(key)
        self.resolve = resolve

        resolved_text = self.resolve_text(view, obj, self.default)
        resolved_date = resolved_text[field_name]

        # If the date resolved to the field name, then return an empty string.
        if resolved_date == self.get_field_name():
            return u''

        # Format the date, if strftime was provided.
        try:
            return resolved_date.strftime(self.strftime)

        except:
            return resolved_date


class DateTime(Date):
    def __init__(self, *args, **kwargs):
        column_filters = kwargs.pop('filters', None)
        if column_filters == None:
            column_filters = [canary_filters.DateTimeRange(), canary_filters.DateEmpty()]
        kwargs['filters'] = column_filters

        Date.__init__(self, *args, **kwargs)


class ColumnHeader(object):
    """
    A header column.
    """
    def __init__(self, column, view, id=None):
        """
        Create a ColumnHeader column.

        Args:
            column: The canary column.
            view: The canary report view.
            id: An attribute to resolve to use to create an HTML id.
        """
        self.column = column

        self.label = self.column.get_label()

        self.view = view

        self.id = id
        if not self.id:
            self.id = u'{name}-header'.format(name=self.column.name)

    def render(self, request):
        """
        Render the header column.

        Args:
            request: The Django request.

        Returns:
            A string containing HTML for the header column.
        """
        output = u'<ul id="{id}" class="th-menu">'.format(id=self.id)

        output += u'<li class="th-menu-title">{label}</li>'.format(
            label=self.label)

        # Render sorts
        for column_sort in self.column.sorts:
            output += u'<li class="th-menu-item">{sort}</li>'.format(
                sort=column_sort.render(request))

        # Render filters
        for column_filter in self.column.filters:
            output += u'<li class="th-menu-item">{filter}</li>'.format(
                filter=column_filter.render(request))

        # Render aggregates
        for column_aggregate in self.column.aggregates:
            output += u'<li class="th-menu-item">{aggregate}</li>'.format(
                aggregate=column_aggregate.render(request))

        return output


def get_column_for_field(field):
    """
    Return an approriate canary column for a Django model field.

    For example, given a Django models.DateField, this method returns a canary
    columns.Date object for the field.

    This is handy for building a canary report view for a Django model.

    Args:
        field: A Django model field.

    Returns:
        A canary column of an appropriate type.
    """
    return Text(field.name)
