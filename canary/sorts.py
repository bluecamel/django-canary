import re
from collections import OrderedDict

from django.template import Context
from django.template.loader import get_template

from forms import SortForm


class SortManager(object):
    """
    It manages the sort.
    """
    form_class = SortForm
    form = None
    columns = None  # Set by the view.
    column_sorts = None
    form_columns = None
    form_extras = None
    view = None

    def __init__(self, view, request, columns):
        """
        Create the SortManager.

        Args:
            view: The canary view.
            request: The Django request.
            columns: A list of column objects.

        Sets:
            view: The canary view.
            request: The Django request.
            columns: A list of column objects.
        """
        self.view = view
        self.request = request
        self.columns = columns

        self.load_sorts()
        self.load_form()
        self.set_sorted_columns()

    def set_sorted_columns(self):
        """
        Build a dictionary of columns that are to be sorted.

        Sets:
            sorted_columns: A dictionary of columns with direction information:
                {'last_name': 'desc', 'first_name': 'desc', 'middle_name': 'desc'}
        """
        sorted_columns = OrderedDict()

        for column_name in self.form_columns:
            column_index = self.form_columns.index(column_name)
            column_direction = self.form_directions[column_index]
            sorted_columns[column_name] = column_direction

        self.sorted_columns = sorted_columns

    def load_form(self):
        """
        Load the form data.

        Args:
            queryset: The Django QuerySet.

        Sets:
            form_columns: An ordered list of columns to be sorted.
            form_directions: An ordered list of directions.
        """
        self.form_columns = []
        self.form_directions = []

        if self.request.method == 'POST':
            self.form = self.form_class(self.request.POST)
            if self.form.is_valid():
                cleaned_data = self.form.cleaned_data
                self.form_columns = cleaned_data['columns'].split(',')
                self.form_directions = cleaned_data['directions'].split(',')

                return

        self.form = self.form_class()

    def load_sorts(self):
        """
        Load all the column sorts.

        Sets:
            column_sorts: A dictionary where the keys are column names and the values are lists of sort objects.
        """
        column_sorts = {}

        for column_name, column in self.columns.iteritems():
            for sort in column.sorts:
                sort.manager = self
                sort.column = column
                self.view.add_extra_media(sort.extra_css, sort.extra_js, sort.js_context)

                for sort_column_name in sort.get_column_names():
                    column_sorts[sort_column_name] = sort

        self.column_sorts = column_sorts

    def sort_queryset(self, queryset):
        """
        Sort the QuerySet.

        Args:
            queryset: The Django QuerySet.

        Returns:
            The Django QuerySet.
        """
        sorted_columns = self.sorted_columns

        order_by_args = []

        for column_name, column_direction in sorted_columns.items():
            column = self.column_sorts.get(column_name, None)
            if column:
                if column_direction == 'asc':
                    order_by_args.append(column_name)

                elif column_direction == 'desc':
                    order_by_args.append('-' + column_name)

        if order_by_args:
            queryset = queryset.order_by(*order_by_args)

        return queryset


class ColumnSort(object):
    """
    The base class of all sorts.
    """
    choices = (
        ('asc', 'ascending'),
        ('desc', 'descending')
    )
    column = None  # Set by the manager.
    column_names = None
    column_labels = None
    extra_css = None
    extra_js = None
    js_context = None
    manager = None
    template_name = 'canary/sorts/column_sort.html'

    def __init__(self, column_names=None, column_labels=None, choices=None):
        """
        Create a ColumnSort.

        Args:
            column_names: An iterable of field names (to be used in a call to QuerySet.order_by).

        Set:
            column_names: A tuple of field names.
            column_labels: A tuple of field labels.
            choices: A tuple of choices (such as Django model choices).
        """
        self.column_names = column_names
        if not self.column_names:
            self.column_names = ()

        self.column_labels = column_labels

        if choices:
            self.choices = choices
        if not self.choices:
            self.choices = ()

    def get_column_names(self):
        """
        Return an iterable of column names.
        """
        column_names = self.column_names
        if len(column_names) < 1:
            column_names = (self.column.get_field_name(),)
        return column_names

    def get_column_labels(self, column_names):
        """
        Return an iterable of column labels.
        """
        column_labels = self.column_labels

        if not column_labels:
            column_labels = []

            for column_name in column_names:
                column_name = re.sub(r'[_-]', ' ', column_name)
                column_labels.append(column_name)

        return column_labels

    def get_display_choices(self):
        """
        Returns:
            A list of tuples, each containing (choice_id, choice, sorted):
                choice_id: ID of the choice (e.g. 'asc', 'desc')
                choice: Label of the choice (e.g. 'Ascending')
                sorted: Boolean indicating whether the choice is active.
        """
        sorted_columns = self.manager.sorted_columns

        choices = []

        column_names = self.get_column_names()
        column_labels = self.get_column_labels(column_names)

        for column_name in column_names:
            field_index = column_names.index(column_name)

            for choice_id, choice in self.choices:
                choice_label = '{label} {choice}'.format(
                    label=column_labels[field_index], choice=choice)

                is_sorted = False
                if self.is_sorted:
                    try:
                        selected_choice_id = sorted_columns[column_name]

                    except:
                        pass

                    else:
                        is_sorted = selected_choice_id == choice_id

                choice = (column_name, choice_id, choice_label, is_sorted)
                choices.append(choice)

        return choices

    @property
    def is_sorted(self):
        """
        A column is considered sorted if any of its field names are found in
        the manager's list of sorted columns.

        Returns:
            A boolean indicating whether the column is sorted.
        """
        sorted_columns = self.manager.sorted_columns

        for column_name in self.get_column_names():
            if column_name in sorted_columns.keys():
                return True

        return False

    def render(self, request):
        """
        Renders the sort controls.

        Args:
            request: The Django request.

        Returns:
            A string of HTML containing the sort controls.  By default,
            this is rendered as part of canary.columns.ColumnHeader.render.
        """
        template = get_template(self.template_name)
        context = {
            'sort': self
        }

        return template.render(Context(context))
