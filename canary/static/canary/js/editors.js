/**
***  jQuery magics to provide editable columns.
**/
$(function() {
    var getFieldUrl = "/inplaceeditform/get_field/";
    var saveURL = "/inplaceeditform/save/";
    var successText = "Successfully saved";
    var inplaceManager = $(".inplaceedit").inplaceeditform({"getFieldUrl": getFieldUrl,
                                                            "saveURL": saveURL,
                                                            "successText": successText});
    inplaceManager.enable();

    $(".inplaceedit").data("applyFinish", function() {return false;});
});
