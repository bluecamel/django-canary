/**
***  Some jQuery love for canary.
***
***  Created by: Branton K. Davis
***  Born on: July 5, 2012
***
**/
var reportOptionsForm = $("form[name=canary-report-options-form]"),

    canaryFilterManager = {
        /**
        ***  Manage filter selections.
        **/

        "filterColumn": function(event, filterInputID, choiceID) {
            /**
            ***  Filter a column.
            **/
            var filterInput = $("#" + filterInputID);

            event.preventDefault();

            filterInput.val(choiceID);

            reportOptionsForm.submit();
        }
    },

    canaryPaginationManager = {
        /**
        ***  Manage pagination selections.
        **/

        "loadPage": function(event, page) {
            /**
            ***
            **/
            var pageInput = $("#id_canary-report-pagination-page");

            event.preventDefault();

            pageInput.val(page);
            reportOptionsForm.submit();
        }
    },

    canarySortManager = {
        /**
        ***  Manage sort selections, particularly order.
        **/

        "getSortColumns": function() {
            /**
            ***  Returns an array of column names.
            **/
            var sortColumnsField = $("#id_canary-report-sort-columns"),
                sortColumns = sortColumnsField.val();

            if (sortColumns.length > 0) {
                return sortColumns.split(",");
            } else {
                return [];
            }
        },

        "getSortDirections": function() {
            /**
            ***  Returns an array of directions.
            **/
            var sortDirectionField = $("#id_canary-report-sort-directions"),
                sortDirections = sortDirectionField.val();

            if (sortDirections.length) {
                return sortDirections.split(",");
            } else {
                return [];
            }
        },

        "updateSortColumn": function(event, action, columnName, columnDirection) {
            /**
            ***  Add, update or remove a sort.
            **/
            var sortLink = $(event.target),
                sortColumns = this.getSortColumns(),
                sortDirections = this.getSortDirections(),
                inSortColumns = $.inArray(columnName, sortColumns);

            event.preventDefault();

            if (inSortColumns > -1) {
                // Remove the column from its current position.
                sortColumns.splice(inSortColumns, 1);
                sortDirections.splice(inSortColumns, 1);
            }

            if (action == "add") {
                sortColumns.push(columnName);
                sortDirections.push(columnDirection);
            }

            this.setSortColumns(sortColumns);
            this.setSortDirections(sortDirections);

            reportOptionsForm.submit();
        },

        "setSortColumns": function(sortColumns) {
            /**
            ***  Set the form field to a comma separated list of column names.
            **/
            var sortColumnsField = $("#id_canary-report-sort-columns");

            sortColumnsField.val(sortColumns.join(","));
        },

        "setSortDirections": function(sortDirections) {
            /**
            ***  Set the form field to a comma separated list of directions.
            **/
            var sortDirectionsField = $("#id_canary-report-sort-directions");

            sortDirectionsField.val(sortDirections.join(","));
        }
    };
