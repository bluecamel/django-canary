/*
 *  A simple plugin that turns lists within th elements into a dropdown menu.
 *
 *  Created by Branton Kyle Davis on April 4, 2012.
 *
 */

(function($){
  $.fn.thMenu = function(options) {
    var settings = $.extend({
        clickable_containers: '.th-menu-overlay'
    }, options);

    return this.each(function() {
        var menu, title, items;

        menu = $(this);
        title = menu.find('.th-menu-title');
        items = menu.find('.th-menu-item');

        /* Copy the menu to a new menu and remove the title. */
        newMenu = menu.clone();
        newMenu.find('.th-menu-title').remove();

        /* Hide menu, and add overlay class. */
        newMenu.hide();
        newMenu.addClass('th-menu-overlay');

        /* Remove the menu items from the original menu. */
        menu.find('.th-menu-item').each(function() {
            $(this).remove();
        });

        /* Position the new menu below the title. */
        menu.after(newMenu);
        newMenu.css({position: 'absolute'});
        /* this caused problems inside Django admin containers.
        newMenu.position({
            my: 'left bottom',
            at: 'left bottom',
            of: menu,
            collision: 'fit'
        });
        */

        /* Make items visible. */
        newMenu.find('.th-menu-item').each(function() {
            $(this).show();
        });

        /* Hide all menus when clicking outside a menu. */
        $('html').click(function(event) {
            if (!$(event.target).hasClass('th-menu-title')) {
                $('.th-menu-overlay').each(function() {
                    $(this).hide();
                });
            }
        });
        $('.th-menu-overlay').click(function(event) {
            // Don't hide if clicked witin a menu overlay.
            event.stopPropagation();
        });
        $('body').delegate(settings.clickable_containers, 'click', function(event) {
            // Don't hide if clicked within containers specified in settings.
            event.stopPropagation();
        });

        /* Toggle menu display when the title is clicked. Hide other menus. */
        title.click(function() {
            var currentMenu = $(this).parent().next();

            /* First, hide all other menus. */
            $('.th-menu-overlay').not(currentMenu).each(function() {
                $(this).hide();
            });

            /* Toggle display of the current menu. */
            currentMenu.toggle();
        });
    });
  };
})(jQuery);