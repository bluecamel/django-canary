from django import forms


class ColumnEditForm(forms.Form):
    id = forms.CharField(max_length=200)
    value = forms.CharField(max_length=255)


class DateRangeForm(forms.Form):
    """
    A simple form to collect start and end dates.
    """
    start_date = forms.DateField(widget=forms.DateInput(attrs={
        'class': 'vDateField', 'size': 10}))
    end_date = forms.DateField(widget=forms.DateInput(attrs={
        'class': 'vDateField', 'size': 10}))


class DateTimeRangeForm(forms.Form):
    """
    A simple form to collect start and end dates.
    """
    start_date = forms.DateField(widget=forms.DateInput(attrs={
        'class': 'vDateField', 'size': 10}), help_text=u'Starting at 12:00 AM')
    end_date = forms.DateField(widget=forms.DateInput(attrs={
        'class': 'vDateField', 'size': 10}), help_text=u'Ending at 11:59 PM')


class PaginationForm(forms.Form):
    """
    A simple form to collect pagination options.
    """
    page = forms.IntegerField(initial=1, widget=forms.HiddenInput())
    rows_per_page = forms.IntegerField(initial=25, widget=forms.HiddenInput())

    def __init__(self, *args, **kwargs):
        """
        Create the PaginationForm.  Define the prefix, if not provided.
        """
        prefix = kwargs.pop('prefix', None)
        if not prefix:
            prefix = 'canary-report-pagination'
        kwargs['prefix'] = prefix

        forms.Form.__init__(self, *args, **kwargs)


class SearchForm(forms.Form):
    """
    A simple form to collect a search term.
    """
    search = forms.CharField()


class SortForm(forms.Form):
    """
    A simple form to collect sort information.
    """
    columns = forms.CharField(max_length=200, required=False,
        widget=forms.widgets.HiddenInput())
    directions = forms.CharField(max_length=200, required=False,
        widget=forms.widgets.HiddenInput())

    def __init__(self, *args, **kwargs):
        """
        Create the SortForm.  Define the prefix, if not provided.
        """
        prefix = kwargs.pop('prefix', None)
        if not prefix:
            prefix = 'canary-report-sort'
        kwargs['prefix'] = prefix

        forms.Form.__init__(self, *args, **kwargs)
