from django.http import QueryDict


def get_query_string(request, set_args=None, del_args=None):
    """
    Ease the handling of creating urls with get queries.

    When building an URL, you often want to only add or remove args from the
    query string.  But you don't want to have to collect the current args every
    time.  So, this method does that for you.

    All you have to do is supply the request, (optionally) the args that you're
    adding, and (optionally) any args that you're removing, and you'll be
    returned the full query string.

    For example:
        Let's assume that you're processing a view that was passed this query
        string: "?s=name&o=desc".

        Now, you need to set the "s" argument to
        "city" instead of "name".  Simply do this:
            url = get_query_string(request, {'s': 'city'})
            # url = '?s=city&o=desc'

        Now, let's say you need another URL the removes the "o" argument:
            url = get_query_string(request, del_args=('o',))
            # url = '?s=city'

        And, of course, you can combine them:
            url = get_query_string(request, {'s': 'city', 'p': 2}, ('o',))
            # url = '?s=city&p=2'

        Or even call it without changing args to get the current query string:
            url = get_query_string(request)
            # url = '?s=name&o=desc'
    """
    if not set_args:
        set_args = {}

    if not del_args:
        del_args = []

    if request is None:
        current_path = ''
        current_query_string = ''
    else:
        current_path = request.path
        current_query_string = request.GET.urlencode()
    get_args = QueryDict(current_query_string, mutable=True)

    # Add new args.
    for key, value in set_args.items():
        get_args[key] = value

    # Delete args.
    for key in del_args:
        try:
            del get_args[key]

        except KeyError:
            pass

    query_string = get_args.urlencode()
    if len(get_args) > 0:
        query_string = '?' + query_string

    return current_path + query_string
