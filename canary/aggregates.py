import locale

from django.db import models


"""
Map strings to the Django aggregate classes.
"""
AGGREGATES = {
    'sum': models.Sum,
    'avg': models.Avg,
    'count': models.Count,
    'min': models.Min,
    'max': models.Max
}


class Aggregate(object):
    aggregate = None
    column = None  # Set by the view.
    column_name = None
    label = None
    type_ = None
    value = None

    def __init__(self, type_, column_name=None, label=None):
        """
        Create an Aggregate.

        Args:
            type_: The type of aggregate (sum, avg, count, min, max)
            column_name: The name of the column to aggregate upon.
            label: The label to display with the aggregate data (Total, etc.).

        Sets:
            type_: The type of aggregate (sum, avg, count, min, max)
            column_name: The name of the column to aggregate upon.
            label: The label to display with the aggregate data (Total, etc.).

        Raises:
            AttributeError: type_ is not one of those mapped in AGGREGATES.
        """
        self.type_ = type_.lower()

        # Make sure type_ is one of those mapped in AGGREGATES.
        if self.type_ not in AGGREGATES.keys():
            raise AttributeError('type_ must be one of {aggregates}.'.format(
                aggregates=', '.join(AGGREGATES)))

        self.aggregate = AGGREGATES[self.type_]

        self.column_name = column_name

        if label:
            self.label = label

    def format_value(self, value):
        return value

    def get_column_name(self):
        if not self.column_name:
            return self.column.get_field_name()
        return self.column_name

    def get_label(self):
        if self.label:
            return self.label
        return self.type_.capitalize()

    def load(self, request, queryset):
        """
        Add the aggregation to the query.

        Args:
            request: The Django request.
            queryset: The Django QuerySet.

        Sets:
            value: The aggregate value.

        Returns:
            queryset: The Django QuerySet.
        """
        column_name = self.get_column_name()
        aggregate_column_name = u'canary_aggregate_{column_name}'.format(
            column_name=column_name)
        kwargs = {}
        kwargs[aggregate_column_name] = self.aggregate(column_name)
        aggregate_data = queryset.aggregate(**kwargs)

        self.value = aggregate_data[aggregate_column_name]

        return queryset

    def render(self, request):
        return u'{label}: {value}'.format(label=self.get_label(),
            value=self.format_value(self.value))


class Dollar(Aggregate):
    def format_value(self, value, force=True):
        try:
            return locale.currency(value, grouping=True)

        except:
            if force:
                # If that failed, try setting the locale and have another go.
                locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')
                return self.format_value(value, force=False)
