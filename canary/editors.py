from django.core.urlresolvers import reverse
from django.template import Context, Template
from django.template.loader import get_template

from canary import columns


class Column(columns.Tag):
    tag = 'div'
    template_name = 'canary/editors/text.html'
    extra_js = {
        'canary/js/editors.min.js': {},
        'inplaceeditform/js/jquery.json.js': {},
        'inplaceeditform/js/jquery.inplaceeditform.js': {}
    }
    js_context = {
        'editColumnURL': '/canary/column_edit/'  # reverse('canary_column_edit')
    }

    def __init__(self, text=None, *args, **kwargs):
        """
        """
        # We want to show the tag even if the column is empty.
        allow_empty = kwargs.pop('allow_empty', True)
        kwargs['allow_empty'] = allow_empty

        # Add the edit class needed for jeditable magics.
        params = kwargs.pop('params', {})
        params_class = params.get('class', None)

        if params_class:
            params_class += ' edit'

        else:
            params_class = 'edit'

        params['class'] = params_class

        kwargs['params'] = params

        columns.Tag.__init__(self, text, *args, **kwargs)

    def get_editable_object(self, view, obj):
        return obj

    def get_render_context(self, view, obj):
        """
        Return a dictionary of values to be included in the template context.

        Args:
            request: The Django request.
            view: The canary view.
            obj: The QuerySet object.

        Returns:
             A dictionary.
        """
        context = columns.Tag.get_render_context(self, view, obj)

        obj = context['obj'] = self.get_editable_object(view, obj)
        context['request'] = view.request

        if obj:
            template = Template('{{% load inplace_edit %}}{{% inplace_edit "obj.{field_name}" %}}'.format(
                field_name=self.get_field_name()))

            context['inplace_edit'] = template.render(Context(context))

        else:
            context['inplace_edit'] = ''

        return context
