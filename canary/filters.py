import datetime

from django.middleware import csrf
from django.db.models import Q
from django.template import Context
from django.template.loader import get_template

import forms as report_forms
#from resolver import Resolver


def get_csrf_token(request):
    """
    Inline replcement for csrf_token templatetag.

    Args:
        request: The Django request.

    Returns:
        A string of HTML containing a CSRF token.
    """
    # TODO: better place for this???
    return u'<div style="display:none"><input type="hidden" \
        name="csrfmiddlewaretoken" value="{token}" /></div>'.format(
            token=csrf.get_token(request))


class ColumnFilter(object):
    """
    The base class of all filters.

    Yes, all your column filter base belong to us...
    """
    column = None  # Set by the view.
    field_name = None
    label = None
    prefix = None
    extra_css = None
    extra_js = None
    js_context = None

    def __init__(self, field_name=None, label=None, prefix=None):
        """
        Create a ColumnFilter.

        Args:
            field_name: The field name to use in filtering the QuerySet.  \
                        If not provided, the column name is used.
            label: The label of the submit button.
            prefix: The prefix to use for naming the HTML object.
        """
        self.field_name = field_name
        self.label = label
        self.prefix = prefix

    def get_field_name(self):
        """
        Get the field name to be used to filter the QuerySet.

        Returns:
            The field_name, if provided, or the column field name.
        """
        if self.field_name:
            return self.field_name
        return self.column.get_field_name()

    def get_label(self):
        """
        Get the filter label.

        Returns:
            The provided label or 'Filter'.
        """
        if self.label:
            return self.label
        return 'Filter'

    def get_prefix_label(self):
        """
        Get the filter prefix label.

        Returns:
            The prefix to be used for each individual filter link.
        """
        return self.get_label() + ' by'

    def get_prefix(self):
        """
        Get the filter prefix.

        Returns:
            The provided prefix or the column name.
        """
        if self.prefix:
            return self.prefix
        return self.column.name  # TODO: Should this be self.get_field_name() ??

    def get_render_context(self, request):
        """
        Get a dictionary for template context.

        Args:
            request: The Django request.

        Raises:
            NotImplementedError: method not defined.
        """
        raise NotImplementedError('You must define a get_render_context method.')

    def load(self, request, queryset):
        """
        Load data for the filter from the current request.

        This method is expected to create the self._meta.data object.

        Args:
            request: The Django Request.
            queryset: The Django QuerySet.

        Sets:
            _meta.data: The data collected from the request object.
        """
        # TODO: Handle more of this in the view so that filters don't have to
        #       worry about request.method.
        raise NotImplementedError('You must define ColumnFilter.load.')

    def render(self, request):
        """
        Renders the filter controls (form, buttons).

        Args:
            request: The Django request.

        Returns:
            A string of HTML containing the filter controls.  By default,
            this is rendered as part of canary.columns.ColumnHeader.render.
        """
        template = get_template(self.template_name)
        context = self.get_render_context(request)
        return template.render(Context(context))


class FormFilter(ColumnFilter):
    """
    The base class of all form filters.  They use a form to get the data used
    to filter the QuerySet.

    Extends canary.filters.ColumnFilter.
    """
    data = None
    form = None  # the form class
    _form = None  # the actual form instance
    template_name = 'canary/filters/form_filter.html'

    def __init__(self, *args, **kwargs):
        """
        Create a FormFilter.
        """
        ColumnFilter.__init__(self, *args, **kwargs)

        if not self.form:
            raise AttributeError('FormFilter.form is undefined.')

    def clear_filter_data(self):
        """
        Clear filter data.

        Sets:
            data: Sets to an empty object.
        """
        # TODO: chane to _meta.data
        self.data = type('data', (object,), {'clear': False})

    def get_render_context(self, request):
        return {
            'form': self._form,
            'prefix': self.get_prefix(),
            'label': self.get_label()
            # TODO: add clear_label init arg and get_clear_label method.
        }

    def get_data(self, request, *args, **kwargs):
        # TODO: chane to _meta.data
        data = {}
        for field_name in self._form.fields:
            data[field_name] = self._form.cleaned_data[field_name]
        return data

    def filter_queryset(self, request, queryset):
        """
        Filter the QuerySet.

        Args:
            request: The Django request.
            queryset: The Django QuerySet.

        Returns:
            A Django QuerySet.

        Raises:
            NotImplementedError: If the method is not overridden by the filter class.
        """
        raise NotImplementedError(
            'You must define a FormFilter.filter_queryset method.')

    def load(self, request, queryset):
        """
        Load data from the request.

        Args:
            request: The Django request.
            queryset: The Django QuerySet.

        Sets:
            data: Populates the object with form data.
        """
        # TODO: change to _meta.data
        self._form = None
        self.form_valid = False

        reset_form = post_has_form_data = False

        prefix = self.get_prefix()

        if request.method == 'POST':
            for field in request.POST.keys():
                if field.find(prefix) >= 0:
                    post_has_form_data = True

            if post_has_form_data:
                clear_key = '{prefix}-filter-clear'.format(prefix=prefix)
                if clear_key in request.POST.keys():
                    reset_form = True

                else:
                    self._form = self.form(request.POST, prefix=prefix)

                    if self.form_valid or self._form.is_valid():
                        self.set_data(request)
                        self.form_valid = True

                    else:
                        reset_form = True

        if reset_form or not post_has_form_data:
            self._form = self.form(prefix=prefix)
            self.clear_filter_data()

    def set_data(self, request, *args, **kwargs):
        """
        Set the form data.

        Args:
            request: The Django request.

        Sets:
            data: The form field data as attributes on an object.
        """
        # TODO: chane to _meta.data
        data = self.get_data(request, *args, **kwargs)

        self.clear_filter_data()

        for field_name in self._form.fields:
            setattr(self.data, field_name, data.get(field_name, None))


class DateRange(FormFilter):
    """
    Filter a date column by a date range.

    Extends canary.filters.FormFilter.
    """
    form = report_forms.DateRangeForm

    def filter_queryset(self, request, queryset):
        """
        Filter the QuerySet.

        Args:
            request: The Django request.
            queryset: The QuerySet.

        Returns:
            A QuerySet.
        """
        if self.form_valid:
            filter_kwargs = {}
            filter_kwargs[self.get_field_name() + '__range'] = [self.data.start_date,
                self.data.end_date]
            queryset = queryset.filter(**filter_kwargs)
        return queryset


class DateTimeRange(FormFilter):
    """
    Filter a datetime column by a date range.

    Extends canary.filters.FormFilter.
    """
    form = report_forms.DateTimeRangeForm

    def filter_queryset(self, request, queryset):
        """
        Filter the QuerySet.

        Args:
            request: The Django request.
            queryset: The QuerySet.

        Returns:
            A QuerySet.
        """
        if self.form_valid:
            start_date = self.data.start_date
            end_date = self.data.end_date

            start_datetime = datetime.datetime(start_date.year,
                start_date.month, start_date.day, 0, 0)
            end_datetime = datetime.datetime(end_date.year,
                end_date.month, end_date.day, 23, 59, 59)

            filter_kwargs = {}
            filter_kwargs[self.get_field_name() + '__range'] = [start_datetime,
                end_datetime]
            queryset = queryset.filter(**filter_kwargs)
        return queryset


class Search(FormFilter):
    """
    Filter a column with an icontains filter.

    Extends canary.filters.FormFilter.
    """
    form = report_forms.SearchForm
    template_name = 'canary/filters/search.html'

    def __init__(self, columns=None, *args, **kwargs):
        """
        Create a Search filter.

        Args:
            columns: A list of column names to search.
        """
        self.columns = columns

    def filter_queryset(self, request, queryset):
        """
        Filter the QuerySet.

        Args:
            request: The Django request.
            queryset: The QuerySet.

        Returns:
            A QuerySet.
        """
        columns = self.columns
        if not columns:
            columns = [self.get_field_name()]

        if self.form_valid:
            # TODO: escape or depend on ORM escaping or use sanitizer

            filter_qs = Q()

            for column in columns:
                q_kwargs = {}
                q_kwargs[column + '__icontains'] = self.data.search
                filter_qs.add(Q(**q_kwargs), Q.OR)

            queryset = queryset.filter(filter_qs)

        return queryset


class Choice(ColumnFilter):
    """
    A Choice filter, for use with Django model fields with choices.

    Extends canary.filters.ColumnFilter.
    """
    template_name = 'canary/filters/choice.html'

    def __init__(self, choices=None, *args, **kwargs):
        """
        Create a Choice filter.

        Sets:
            choices: A tuple of choices.
        """
        ColumnFilter.__init__(self, *args, **kwargs)

        self.choices = choices
        if not self.choices:
            self.choices = ()

    def filter_queryset(self, request, queryset):
        """
        Filter the QuerySet based on the selected choice.

        Args:
            request: The Django request.
            queryset: The QuerySet.

        Returns:
            A QuerySet.
        """
        filter_name = self.get_filter_name()
        field_name = self.get_field_name()
        filter_value = getattr(self.data, filter_name)

        if filter_value:
            filter_kwargs = {field_name: filter_value}
            queryset = queryset.filter(**filter_kwargs)

        return queryset

    def get_choices(self, queryset):
        raise NotImplementedError('You must define a get_choices method on a choice filter (for now).')

    #def get_choices(self, queryset):
    #    """
    #    Get the choice tuple.

    #    Args:
    #        queryset: The QuerySet.

    #    Returns:
    #        A choice tuple (exactly the same as used to define Django model
    #        fields).
    #    """
    #    # TODO: This doesn't work for a field_name like 'company__industry'
    #    choices = ()
    #    field_name = self.get_field_name()

    #    for field in queryset.model._meta.fields:
    #        if field.name == field_name:
    #            try:
    #                choices = field.choices

    #            except:
    #                choices = ()

    #    return choices

    def get_data(self, request, *args, **kwargs):
        """
        Get the filter data.

        Args:
            request: The Django request.

        Returns:
            A dictionary of form data.
        """
        filter_name = self.get_filter_name()

        data = {}

        if filter_name in request.POST:
            data[filter_name] = request.POST[filter_name]
        else:
            data[filter_name] = None

        return data

    def get_render_context(self, request):
        context = {}
        context['filter'] = self
        context['filter_name'] = self.get_filter_name()
        context['filter_input_id'] = 'choice-filter-{filter_name}'.format(
            **context)

        # Get the filter value.
        filter_value = getattr(self.data, context['filter_name'], None)
        if not filter_value:
            filter_value = u''

        # Check the type of the choices.
        if len(self.choices) > 0:
            first_choice_id, first_choice_value = self.choices[0]
            if isinstance(first_choice_id, int):
                try:
                    filter_value = int(filter_value)

                except:
                    filter_value = u''
        context['filter_value'] = filter_value

        return context

    def get_filter_name(self):
        """
        Get the filter name.

        Returns:
            The name of the HTML form input element.
        """
        prefix = self.get_prefix()
        return '{prefix}-choice-filter'.format(prefix=prefix)

    def load(self, request, queryset):
        """
        Load filter data.

        Args:
            request: The Django request.

        Sets:
            choices: A tuple of choices.
            data: And object with the filter data as attributes.
        """
        if len(self.choices) < 1:
            self.choices = self.get_choices(queryset)

        # TODO: change from data to _meta.data
        self.set_data(request)

    def set_data(self, request):
        """
        Set the filter data.

        Args:
            request: The Django request.

        Sets:
            data: An object with the filter data as attributes.
        """
        # TODO: change from data to _meta.data
        attr_dict = self.get_data(request)

        self.data = type('data', (object,), attr_dict)


class DateEmpty(Choice):
    """
    Filter a date column by empty/existing.

    Extends canary.filters.Choice.
    """

    def get_choices(self, queryset):
        column_label = self.column.get_label()

        choices = (
            (0, column_label + ' empty'),
            (1, column_label + ' exists')
        )
        return choices

    def get_prefix(self):
        prefix = '{prefix}-date-empty-'.format(
            prefix=Choice.get_prefix(self))
        return prefix

    def filter_queryset(self, request, queryset):
        # TODO: These three lines are repeated.  Should be a filter method?
        filter_name = self.get_filter_name()
        field_name = self.get_field_name()
        filter_value = getattr(self.data, filter_name)

        if filter_value == u'0':
            queryset = queryset.filter(**{field_name + '__isnull': True})

        elif filter_value == u'1':
            queryset = queryset.filter(**{field_name + '__isnull': False})

        return queryset


class ForeignKey(Choice):
    """
    A ForeignKey filter, for use with Django models.ForeignKey fields.

    Extends canary.filters.Choice.
    """

    def get_choices(self, queryset):
        """
        Get the choice tuple.

        Args:
            queryset: The QuerySet.

        Returns:
            A choice tuple (exactly the same as used to define Django model
            fields).
        """
        # TODO: Need to make this mor automatical and find related options
        # from the QuerySet.
        choices = []
        #field_name = self.get_field_name()
        #model = queryset.model
        related_model = self.get_related_model()

        for related_obj in related_model.objects.all():
            # TODO: accept an attribute name for object display.
            choices.append((related_obj.id, unicode(related_obj)))

        choices = tuple(choices)

        #print 'model', model
        #print 'field_name', field_name
        #print 'resolved', Resolver(model).resolve(field_name)
        # TODO: ChoiceFilter could probably work like this too.
        #model_field = queryset.model
        # Title.indie_reviews.related.model._meta.fields
        # something to traverse and get here??  Resolver?

        return choices

    def get_related_model(self):  # TODO: need to create get_filter_queryset (e.g. subset of Users)
        """
        Get the related Django model.

        Returns:
            The related Django model.

        Raises:
            NotImplementedError: Magic is hard. Until I figure out how to work related magics, you must define this.
        """
        raise NotImplementedError(u'Magic is hard. Until I figure out how to work related magics, you must define this.')
