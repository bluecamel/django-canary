from django.contrib import admin

from models import Contact, Company


class CompanyAdmin(admin.ModelAdmin):
    list_display = ('name', 'industry')
    list_filter = ('industry',)
    search_fields = ('name',)


class ContactAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'middle_name', 'last_name', 'active', 'email',
        'company', 'company_industry')
    list_filter = ('active', 'company', 'company__industry')
    search_fields = ('first_name', 'middle_name', 'last_name')

    def company_industry(self, contact):
        return contact.company.get_industry_display()
    company_industry.short_description = 'Industry'


admin.site.register(Company, CompanyAdmin)
admin.site.register(Contact, ContactAdmin)
