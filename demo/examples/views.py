from django.views.generic import TemplateView

from canary import columns
from canary import editors
from canary import filters as canary_filters
from canary import sorts as canary_sorts
from canary.views import ModelReport, QueryReport

from models import Contact, Company


class Home(TemplateView):
    template_name = 'examples/home.html'

home = Home.as_view()


class ContactsAll(ModelReport):
    class Meta:
        model = Contact

contacts_all = ContactsAll.as_view()


class ContactsActive(ModelReport):
    class Meta:
        model = Contact
        queryset = Contact.objects.filter(active=True)

contacts_active = ContactsActive.as_view()


class ContactsActiveInclude(ModelReport):
    class Meta:
        model = Contact
        queryset = Contact.objects.filter(active=True)
        include = ('first_name', 'middle_name', 'last_name', 'company', 'created', 'updated')

contacts_active_include = ContactsActiveInclude.as_view()


class ContactsActiveExclude(ModelReport):
    class Meta:
        model = Contact
        queryset = Contact.objects.filter(active=True)
        exclude = ('active',)

contacts_active_exclude = ContactsActiveExclude.as_view()


class ContactsActiveColumns(QueryReport):
    first_name = columns.Text()
    middle_name = columns.Text()
    last_name = columns.Text()

    industry = columns.Text('company__get_industry_display')

    class Meta:
        queryset = Contact.objects.filter(active=True)

contacts_active_columns = ContactsActiveColumns.as_view()


class CompanyForeignKey(canary_filters.ForeignKey):
    def get_related_model(self):
        return Company


class ContactsActiveViewMethod(QueryReport):
    first_name = editors.Column()
    full_name = columns.Text('get_full_name',
        filters=[canary_filters.Search(
            ('first_name', 'middle_name', 'last_name'))],
        sorts=[canary_sorts.ColumnSort(
            column_names=('first_name', 'last_name', 'middle_name'),
            column_labels=('first name', 'last name', 'middle name'))])
    email = columns.EmailLink()
    company = editors.Column('company', filters=[CompanyForeignKey()],
        sorts=[canary_sorts.ColumnSort(
            column_names=('company__name', ),
            column_labels=('company name',))])
    active = columns.BooleanValue()
    company_site = columns.Link(url='company__corporate_site', text='company__name')
    industry = columns.Text('get_industry',
        filters=[canary_filters.Choice(
            Company.INDUSTRY_CHOICES, 'company__industry')])

    def get_industry(self, contact):
        """
        The first argument is passed the current object from the QuerySet.  You can return anything you need.
        """
        return contact.company.get_industry_display()

    class Meta:
        queryset = Contact.objects.filter(active=True)

contacts_active_view_method = ContactsActiveViewMethod.as_view()


class CompanyAll(QueryReport):
    name = editors.Column()
    industry = columns.Text('get_industry_display',
        filters=[canary_filters.Choice(
            Company.INDUSTRY_CHOICES, 'industry')])
    corporate_site = columns.Link()

    class Meta:
        queryset = Company.objects.all()

company_all = CompanyAll.as_view()
