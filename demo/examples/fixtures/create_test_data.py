from random import choice

from examples.models import Company, Contact


BOOLEAN_CHOICES = (False, True)

COMPANY_NAME_PREFIXES = ('Plastico', 'Camel', 'Fuzwiz', 'Hoodillers', 'West')
COMPANY_NAME_SUFFIXES = ('International', 'GMBH', 'Inc.', 'Holdings', 'Farms',
    'Grocers')

FIRST_NAME_PREFIXES = ('Ae', 'Di', 'Mo', 'Fam')
FIRST_NAME_SUFFIXES = ('dar', 'kil', 'glar', 'tres')

LAST_NAME_PREFIXES = ('Fo', 'Bi', 'Mo', 'Lu')
LAST_NAME_SUFFIXES = ('rilz', 'dington', 'umpery', 's')

EMAIL_DOMAINS = ('gmail.com', 'att.net', 'yahoo.com', 'hotmail.com')


def concat_choices(sep, *choices):
    chosen = [choice(lucky) for lucky in choices]
    return sep.join(chosen)


def create_random_company():
    name = concat_choices(' ', COMPANY_NAME_PREFIXES, COMPANY_NAME_SUFFIXES)
    industry = choice(dict(Company.INDUSTRY_CHOICES).keys())

    # Make sure the company name isn't already used.
    company_qs = Company.objects.filter(name=name)
    if company_qs.count() > 0:
        return None

    corporate_site = 'http://www.' + name.replace(' ', '').lower() + '.com/'

    company = Company(name=name, industry=industry,
        corporate_site=corporate_site)
    company.save()

    return company


def create_random_contact():
    first_name = concat_choices('', FIRST_NAME_PREFIXES, FIRST_NAME_SUFFIXES)
    middle_name = concat_choices('', FIRST_NAME_PREFIXES, FIRST_NAME_SUFFIXES)
    last_name = concat_choices('', LAST_NAME_PREFIXES, LAST_NAME_SUFFIXES)

    # Make sure the name isn't already used.
    contact_qs = Contact.objects.filter(first_name=first_name,
        middle_name=middle_name, last_name=last_name)
    if contact_qs.count() > 0:
        return None

    # Make up a fake e-mail from the name.
    email = '.'.join([first_name, middle_name, last_name])
    email += '@' + choice(EMAIL_DOMAINS)
    email = email.lower()

    contact = Contact(first_name=first_name, middle_name=middle_name,
        last_name=last_name, active=choice(BOOLEAN_CHOICES), email=email,
        company=Company.objects.all().order_by('?')[0])
    contact.save()

    return contact
