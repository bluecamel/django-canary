from django.conf.urls.defaults import patterns, url
from django.contrib.admin.views.decorators import staff_member_required

import views


urlpatterns = patterns('',
    url(r'^$', views.home, name='home'),
    url(r'^contact/all/$', staff_member_required(
        views.contacts_all), name='contacts_all'),
    url(r'^contact/active/$', staff_member_required(
        views.contacts_active), name='contacts_active'),
    url(r'^contact/active/include/$', staff_member_required(
        views.contacts_active_include), name='contacts_active_include'),
    url(r'^contact/active/exclude/$', staff_member_required(
        views.contacts_active_exclude), name='contacts_active_exclude'),
    url(r'^contact/active/columns/$', staff_member_required(
        views.contacts_active_columns), name='contacts_active_columns'),
    url(r'^contact/active/view_method/$', staff_member_required(
        views.contacts_active_view_method), name='contacts_active_view_method'),
    url(r'^company/all/$', staff_member_required(
        views.company_all), name='company_all')
)
