from django.db import models


class Company(models.Model):
    # constants
    INDUSTRY_CHOICES = (
        (0, 'Technology'),
        (1, 'Software'),
        (2, 'Bananas'),
        (3, 'Hardware'),
        (4, 'Gizmos'),
        (5, 'Hoodillery')
    )

    # columns
    name = models.CharField(max_length=100)
    industry = models.IntegerField(max_length=1, choices=INDUSTRY_CHOICES)
    corporate_site = models.URLField(blank=True, null=True)

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name_plural = u'Companies'


class Contact(models.Model):
    first_name = models.CharField(max_length=100)
    middle_name = models.CharField(max_length=100, blank=True, null=True)
    last_name = models.CharField(max_length=100)

    email = models.EmailField()

    active = models.BooleanField(default=True)

    company = models.ForeignKey(Company)

    def __unicode__(self):
        return unicode(self.get_full_name())

    def get_full_name(self):
        """
            Return a string containing the contact's full name.
        """
        name_fields = []
        for name_field in (self.first_name, self.middle_name, self.last_name):
            if name_field:
                name_fields.append(name_field)
        return ' '.join(name_fields)

    @property
    def has_first_name(self):
        """
            Return a boolean, indicating whether the contact has a middle name.
        """
        return bool(self.middle_name)
