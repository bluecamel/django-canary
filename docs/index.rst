.. canary documentation master file, created by
   sphinx-quickstart on Fri Jul  6 19:59:29 2012.

Welcome to django canary!
=========================================

Contents:

.. toctree::

   installation
   introduction
   views
   columns
   filters
   sorts
   resolver
   qitya

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

