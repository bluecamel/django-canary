Installation
=========================================

=========================
Installing Canary Reports
=========================

It's very easy to install Canary Reports:

1. Download the source (`available on bitbucket <https://bitbucket.org/bluecamel/django-canary/>`_) and put the canary directory somewhere in your Python path::

    $ git clone git@bitbucket.org:bluecamel/django-canary.git django-canary

2. Add ``canary`` to ``INSTALLED_APPS``.

3. Run ``collectstatic`` or copy the ``static/canary`` directory to your static media directory.

=========================
Running the Demo Project
=========================

All of the examples in the documentation are included in the ``demo`` project.  If you'd like to play with the examples yourself, simply:

1. Make sure ``canary`` and ``canary.demo`` are in your ``PYTHONPATH`` or ``sys.path``.

2. Install ``inplaceeditform``. This is a dependency for editable columns:

    - Make sure ``inplaceeditform`` is in your ``PYTHONPATH`` or ``sys.path``.
    - Make sure you've run ``collectstatic`` or otherwise managed your static files.

3. Change the database configuration in ``demo/settings.py`` to suit your preferences.  If you have `sqlite` installed, you don't need to do anything.

4. Create the database tables needed for the app::

    $ python manage.py syncdb

5. Load initial data to use to play with the reports::

    $ python manage.py loaddata canary_test_data

6. Start your development server::

    $ python manage.py runserver

7. Go to http://localhost/canary_examples/ to play with the reports.

============================
Installing the Examples App
============================

You can also just install the ``examples`` app in your own project:

1. Add ``canary.demo.examples`` to ``INSTALLED_APPS``.

2. Create the database tables needed for the app::

    $ python manage.py syncdb

3. Add this line to your urls.py::

    url(r'canary_examples/', include('canary.demo.examples.urls')),

4. Start your development server::

    $ python mnage.py runserver

5. Go to http://localhost/canary_examples/ to play with the reports.


==========
What now?
==========

Read through the :doc:`Introduction to Django Canary Reporting <introduction>`.
