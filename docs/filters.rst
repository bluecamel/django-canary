Filters
=========================================

More info on filters (and how to create your own!!) coming soon...

================
Class Reference
================

* :ref:`canary.filters.ColumnFilter <canary-filters-columnfilter>`
* :ref:`canary.filters.FormFilter <canary-filters-formfilter>`
* :ref:`canary.filters.DateRange <canary-filters-daterange>`
* :ref:`canary.filters.Search <canary-filters-search>`
* :ref:`canary.filters.Choice <canary-filters-choice>`
* :ref:`canary.filters.ForeignKey <canary-filters-foreignkey>`

.. automodule:: canary.filters

.. _canary-filters-columnfilter:
.. autoclass:: ColumnFilter
    :members: __init__, get_field_name, load

.. _canary-filters-formfilter:
.. autoclass:: FormFilter
    :members: __init__, clear_filter_data, get_data, get_label, get_prefix, filter_queryset, load, render, set_data

.. _canary-filters-daterange:
.. autoclass:: DateRange
    :members: filter_queryset

.. _canary-filters-search:
.. autoclass:: Search
    :members: __init__, filter_queryset

.. _canary-filters-choice:
.. autoclass:: Choice
    :members: filter_queryset, get_choices, get_data, get_filter_name, load, render, set_data

.. _canary-filters-foreignkey:
.. autoclass:: ForeignKey
    :members: filter_queryset, get_choices, get_related_model
