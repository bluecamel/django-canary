Questions I Think You'll Ask
=============================

==========================
What's with the canary???
==========================

I was running out of ideas and the thesaurus was running dry.  I was trying to think of names that could have a fun logo but related to reporting somehow.  I had gone through **Sarge** (honestly...eek) and almost went with **django informer** before I decided to find a tool to help me find related words.  So I found `OneLook's Reverse Dictionary <http://www.onelook.com/reverse-dictionary.shtml>`_.  It's a bit plain, but is definitely going to be a fixture in my brainstorming shelf.

So, doing a reverse search on **informer** gave me **canary**.  And I figure that bird logos haven't done other folks harm.  Besides, the cornier part of me digs the *canary in a coal mine* idea, where your data is the coal mine.  Hopefully the ease of creating new reports will help people learn more from their data.

Also, the name that I'd been using was **basic_reports**, which is clearly lame, not to mention increasingly inaccurate as I was seeing more potential in the setup and adding more bells.
