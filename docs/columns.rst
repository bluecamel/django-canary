Columns
=========================================

================
Class Reference
================

* :ref:`canary.columns.Column <canary-columns-column>`
* :ref:`canary.columns.Text <canary-columns-text>`
* :ref:`canary.columns.Tag <canary-columns-tag>`
* :ref:`canary.columns.Link <canary-columns-link>`
* :ref:`canary.columns.ReverseLink <canary-columns-reverselink>`
* :ref:`canary.columns.AdminLink <canary-columns-adminlink>`
* :ref:`canary.columns.BooleanValue <canary-columns-booleanvalue>`
* :ref:`canary.columns.Date <canary-columns-date>`
* :ref:`canary.columns.EmailLink <canary-columns-emaillink>`
* :ref:`canary.columns.ColumnHeader <canary-columns-columnheader>`
* :ref:`canary.columns.get_column_for_field <canary-columns-get-column-for-field>`

.. automodule:: canary.columns

.. _canary-columns-column:
.. autoclass:: Column
    :members: __init__

.. _canary-columns-text:
.. autoclass:: Text
    :members: __init__, get_field_name, get_sort_field, resolve_text, render

.. _canary-columns-tag:
.. autoclass:: Tag
    :members: __init__, render

.. _canary-columns-link:
.. autoclass:: Link
    :members: __init__, render

.. _canary-columns-reverselink:
.. autoclass:: ReverseLink
    :members: __init__, render

.. _canary-columns-adminlink:
.. autoclass:: AdminLink
    :members: __init__, render

.. _canary-columns-booleanvalue:
.. autoclass:: BooleanValue
    :members: __init__, resolve_text

.. _canary-columns-date:
.. autoclass:: Date
    :members: __init__

.. _canary-columns-emaillink:
.. autoclass:: EmailLink
    :members: __init__, resolve_text

.. _canary-columns-columnheader:
.. autoclass:: ColumnHeader
    :members: __init__, render

.. _canary-columns-get-column-for-field:
.. automethod:: canary.columns.get_column_for_field
