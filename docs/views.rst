Views
=========================================

More information about views coming soon...

================
Class Reference
================

* :ref:`canary.views.BasicReport <canary-views-basicreport>`
* :ref:`canary.views.QueryReport <canary-views-queryreport>`
* :ref:`canary.views.ModelReport <canary-views-modelreport>`

.. automodule:: canary.views

.. _canary-views-basicreport:
.. autoclass:: BasicReport
    :members:

.. _canary-views-queryreport:
.. autoclass:: QueryReport
    :members:

.. _canary-views-modelreport:
.. autoclass:: ModelReport
    :members:
