�cdocutils.nodes
document
q)�q}q(U	nametypesq}q(X   filtersqNX   django's admin reversing docsq�X'   introduction to django canary reportingqNX    a little more advanced (columns)q	NX   getting started (views)q
NuUsubstitution_defsq}qUparse_messagesq]qUcurrent_sourceqNU
decorationqNUautofootnote_startqKUnameidsq}q(hUfiltersqhUdjango-s-admin-reversing-docsqhU'introduction-to-django-canary-reportingqh	Ua-little-more-advanced-columnsqh
Ugetting-started-viewsquUchildrenq]qcdocutils.nodes
section
q)�q}q(U	rawsourceqU UparentqhUsourceq cdocutils.nodes
reprunicode
q!X3   /Users/bdavis/D/django-canary/docs/introduction.rstq"��q#}q$bUtagnameq%Usectionq&U
attributesq'}q((Udupnamesq)]Uclassesq*]Ubackrefsq+]Uidsq,]q-haUnamesq.]q/hauUlineq0KUdocumentq1hh]q2(cdocutils.nodes
title
q3)�q4}q5(hX'   Introduction to Django Canary Reportingq6hhh h#h%Utitleq7h'}q8(h)]h*]h+]h,]h.]uh0Kh1hh]q9cdocutils.nodes
Text
q:X'   Introduction to Django Canary Reportingq;��q<}q=(hh6hh4ubaubcdocutils.nodes
paragraph
q>)�q?}q@(hX  This project is intended to allow you to build simple reports using models or QuerySets.  You get quite a bit of functionality for free (choice filters, foreign key filters, multi-column sorting and pagination).  However, you can do much more by extending the built-in columns, filters, sorts, or even views, which control report generation.  The default styling is very simple, and intended to blend in with the default Django admin, but the templates are also very simple and therefore easy to extend or replace.qAhhh h#h%U	paragraphqBh'}qC(h)]h*]h+]h,]h.]uh0Kh1hh]qDh:X  This project is intended to allow you to build simple reports using models or QuerySets.  You get quite a bit of functionality for free (choice filters, foreign key filters, multi-column sorting and pagination).  However, you can do much more by extending the built-in columns, filters, sorts, or even views, which control report generation.  The default styling is very simple, and intended to blend in with the default Django admin, but the templates are also very simple and therefore easy to extend or replace.qE��qF}qG(hhAhh?ubaubh)�qH}qI(hU hhh h#h%h&h'}qJ(h)]h*]h+]h,]qKhah.]qLh
auh0Kh1hh]qM(h3)�qN}qO(hX   Getting started (Views)qPhhHh h#h%h7h'}qQ(h)]h*]h+]h,]h.]uh0Kh1hh]qRh:X   Getting started (Views)qS��qT}qU(hhPhhNubaubh>)�qV}qW(hX1   Assume that we have the following Django models::qXhhHh h#h%hBh'}qY(h)]h*]h+]h,]h.]uh0K
h1hh]qZh:X0   Assume that we have the following Django models:q[��q\}q](hX0   Assume that we have the following Django models:hhVubaubcdocutils.nodes
literal_block
q^)�q_}q`(hX`  from django.db import models


class Company(models.Model):
    # constants
    INDUSTRY_CHOICE_TECHNOLOGY = 1
    INDUSTRY_CHOICE_MONKEYS = 2
    INDUSTRY_CHOICE_TECHNOLOGY_MONKEYS = 3
    INDUSTRY_CHOICE_MONKEY_TECHNOLOGY = 4
    INDUSTRY_CHOICES = (
        (INDUSTRY_CHOICE_TECHNOLOGY, 'Technology'),
        (INDUSTRY_CHOICE_MONKEYS, 'Monkeys'),
        (INDUSTRY_CHOICE_TECHNOLOGY_MONKEYS, 'Technology Monkeys'),
        (INDUSTRY_CHOICE_MONKEY_TECHNOLOGY, 'Monkey Technology')
    )

    # columns
    name = models.CharField(max_length=100)
    industry = models.IntegerField(max_length=1, choices=INDUSTRY_CHOICES)

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name_plural = u'Companies'


class Contact(models.Model):
    first_name = models.CharField(max_length=100)
    middle_name = models.CharField(max_length=100, blank=True, null=True)
    last_name = models.CharField(max_length=100)

    active = models.BooleanField(default=True)

    company = models.ForeignKey(Company)

    def __unicode__(self):
        return unicode(self.get_full_name())

    def get_full_name(self):
        """
            Return a string containing the contact's full name.
        """
        name_fields = []
        for name_field in (self.first_name, self.middle_name, self.last_name):
            if name_field:
                name_fields.append(name_field)
        return ' '.join(name_fields)

    @property
    def has_first_name(self):
        """
            Return a boolean, indicating whether the contact has a middle name.
        """
        return bool(self.middle_name)hhHh h#h%Uliteral_blockqah'}qb(U	xml:spaceqcUpreserveqdh,]h+]h)]h*]h.]uh0Kh1hh]qeh:X`  from django.db import models


class Company(models.Model):
    # constants
    INDUSTRY_CHOICE_TECHNOLOGY = 1
    INDUSTRY_CHOICE_MONKEYS = 2
    INDUSTRY_CHOICE_TECHNOLOGY_MONKEYS = 3
    INDUSTRY_CHOICE_MONKEY_TECHNOLOGY = 4
    INDUSTRY_CHOICES = (
        (INDUSTRY_CHOICE_TECHNOLOGY, 'Technology'),
        (INDUSTRY_CHOICE_MONKEYS, 'Monkeys'),
        (INDUSTRY_CHOICE_TECHNOLOGY_MONKEYS, 'Technology Monkeys'),
        (INDUSTRY_CHOICE_MONKEY_TECHNOLOGY, 'Monkey Technology')
    )

    # columns
    name = models.CharField(max_length=100)
    industry = models.IntegerField(max_length=1, choices=INDUSTRY_CHOICES)

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name_plural = u'Companies'


class Contact(models.Model):
    first_name = models.CharField(max_length=100)
    middle_name = models.CharField(max_length=100, blank=True, null=True)
    last_name = models.CharField(max_length=100)

    active = models.BooleanField(default=True)

    company = models.ForeignKey(Company)

    def __unicode__(self):
        return unicode(self.get_full_name())

    def get_full_name(self):
        """
            Return a string containing the contact's full name.
        """
        name_fields = []
        for name_field in (self.first_name, self.middle_name, self.last_name):
            if name_field:
                name_fields.append(name_field)
        return ' '.join(name_fields)

    @property
    def has_first_name(self):
        """
            Return a boolean, indicating whether the contact has a middle name.
        """
        return bool(self.middle_name)qf��qg}qh(hU hh_ubaubh>)�qi}qj(hX�   To create a simple report using these models, just create a report view.  Put this in your views.py (or maybe admin_views.py or reports.py).  We'll start with the most basic option, which is a ModelReport::qkhhHh h#h%hBh'}ql(h)]h*]h+]h,]h.]uh0KDh1hh]qmh:X�   To create a simple report using these models, just create a report view.  Put this in your views.py (or maybe admin_views.py or reports.py).  We'll start with the most basic option, which is a ModelReport:qn��qo}qp(hX�   To create a simple report using these models, just create a report view.  Put this in your views.py (or maybe admin_views.py or reports.py).  We'll start with the most basic option, which is a ModelReport:hhiubaubh^)�qq}qr(hX�   from canary.views import ModelReport

from models import Contact


class ContactsAll(ModelReport):
    class Meta:
        model = Contact

contacts_all = ContactsAll.as_view()hhHh h#h%hah'}qs(hchdh,]h+]h)]h*]h.]uh0KFh1hh]qth:X�   from canary.views import ModelReport

from models import Contact


class ContactsAll(ModelReport):
    class Meta:
        model = Contact

contacts_all = ContactsAll.as_view()qu��qv}qw(hU hhqubaubh>)�qx}qy(hX�   There's nothing terribly special about the view handling.  All of the report views in the package are extensions of Django's ``TemplateView``, so you can configure your URLs in whatever way you like.  Here, we'll setup our urls.py like so::qzhhHh h#h%hBh'}q{(h)]h*]h+]h,]h.]uh0KQh1hh]q|(h:X}   There's nothing terribly special about the view handling.  All of the report views in the package are extensions of Django's q}��q~}q(hX}   There's nothing terribly special about the view handling.  All of the report views in the package are extensions of Django's hhxubcdocutils.nodes
literal
q�)�q�}q�(hX   ``TemplateView``h'}q�(h)]h*]h+]h,]h.]uhhxh]q�h:X   TemplateViewq���q�}q�(hU hh�ubah%Uliteralq�ubh:Xb   , so you can configure your URLs in whatever way you like.  Here, we'll setup our urls.py like so:q���q�}q�(hXb   , so you can configure your URLs in whatever way you like.  Here, we'll setup our urls.py like so:hhxubeubh^)�q�}q�(hX  from django.conf.urls.defaults import patterns, url
from django.contrib.admin.views.decorators import staff_member_required

import views


urlpatterns = patterns('',
    url(r'^contacts/all/$', staff_member_required(
        views.contacts_all), name='contacts_all'),
)hhHh h#h%hah'}q�(hchdh,]h+]h)]h*]h.]uh0KSh1hh]q�h:X  from django.conf.urls.defaults import patterns, url
from django.contrib.admin.views.decorators import staff_member_required

import views


urlpatterns = patterns('',
    url(r'^contacts/all/$', staff_member_required(
        views.contacts_all), name='contacts_all'),
)q���q�}q�(hU hh�ubaubh>)�q�}q�(hX�   This will create a report containing all columns in the Contact model, each sortable and searchable.  Not bad for just a few lines of code.q�hhHh h#h%hBh'}q�(h)]h*]h+]h,]h.]uh0K_h1hh]q�h:X�   This will create a report containing all columns in the Contact model, each sortable and searchable.  Not bad for just a few lines of code.q���q�}q�(hh�hh�ubaubh>)�q�}q�(hX�   Now, let's say that we want to have more control over the queryset used to generate the report.  Maybe this report should only include active contacts.  Well, this is easy to do by giving the queryset to the view.  Our new view might look like this::q�hhHh h#h%hBh'}q�(h)]h*]h+]h,]h.]uh0Kah1hh]q�h:X�   Now, let's say that we want to have more control over the queryset used to generate the report.  Maybe this report should only include active contacts.  Well, this is easy to do by giving the queryset to the view.  Our new view might look like this:q���q�}q�(hX�   Now, let's say that we want to have more control over the queryset used to generate the report.  Maybe this report should only include active contacts.  Well, this is easy to do by giving the queryset to the view.  Our new view might look like this:hh�ubaubh^)�q�}q�(hX�   class ContactsActive(ModelReport):
    class Meta:
        model = Contact
        queryset = Contact.objects.filter(active=True)hhHh h#h%hah'}q�(hchdh,]h+]h)]h*]h.]uh0Kch1hh]q�h:X�   class ContactsActive(ModelReport):
    class Meta:
        model = Contact
        queryset = Contact.objects.filter(active=True)q���q�}q�(hU hh�ubaubh>)�q�}q�(hX�   What if we didn't want the report to display all of the columns from the model?  Well, that's easy too, and very similar to Django's ModelAdmin or ModelForm.  You can either specify the columns that you want to include in the report::q�hhHh h#h%hBh'}q�(h)]h*]h+]h,]h.]uh0Khh1hh]q�h:X�   What if we didn't want the report to display all of the columns from the model?  Well, that's easy too, and very similar to Django's ModelAdmin or ModelForm.  You can either specify the columns that you want to include in the report:q���q�}q�(hX�   What if we didn't want the report to display all of the columns from the model?  Well, that's easy too, and very similar to Django's ModelAdmin or ModelForm.  You can either specify the columns that you want to include in the report:hh�ubaubh^)�q�}q�(hX�   class ContactsActiveInclude(ModelReport):
    class Meta:
        model = Contact
        queryset = Contact.objects.filter(active=True)
        include = ('first_name', 'middle_name', 'last_name', 'company', 'created', 'updated')hhHh h#h%hah'}q�(hchdh,]h+]h)]h*]h.]uh0Kjh1hh]q�h:X�   class ContactsActiveInclude(ModelReport):
    class Meta:
        model = Contact
        queryset = Contact.objects.filter(active=True)
        include = ('first_name', 'middle_name', 'last_name', 'company', 'created', 'updated')q���q�}q�(hU hh�ubaubh>)�q�}q�(hX9   Or you can specify the columns that you want to exclude::q�hhHh h#h%hBh'}q�(h)]h*]h+]h,]h.]uh0Kph1hh]q�h:X8   Or you can specify the columns that you want to exclude:q���q�}q�(hX8   Or you can specify the columns that you want to exclude:hh�ubaubh^)�q�}q�(hX�   class ContactsActiveExclude(ModelReport):
    class Meta:
        model = Contact
        queryset = Contact.objects.filter(active=True)
        exclude = ('active',)hhHh h#h%hah'}q�(hchdh,]h+]h)]h*]h.]uh0Krh1hh]q�h:X�   class ContactsActiveExclude(ModelReport):
    class Meta:
        model = Contact
        queryset = Contact.objects.filter(active=True)
        exclude = ('active',)qŅ�q�}q�(hU hh�ubaubh>)�q�}q�(hXY   Both of the previous examples have the same result of including all columns but 'active'.q�hhHh h#h%hBh'}q�(h)]h*]h+]h,]h.]uh0Kxh1hh]q�h:XY   Both of the previous examples have the same result of including all columns but 'active'.qͅ�q�}q�(hh�hh�ubaubeubh)�q�}q�(hU hhh h#h%h&h'}q�(h)]h*]h+]h,]q�hah.]q�h	auh0K|h1hh]q�(h3)�q�}q�(hX    A little more advanced (Columns)q�hh�h h#h%h7h'}q�(h)]h*]h+]h,]h.]uh0K|h1hh]q�h:X    A little more advanced (Columns)qۅ�q�}q�(hh�hh�ubaubh>)�q�}q�(hXY  Now, let's say that we wanted to add the industry column from the Company field.  The quickest way to do this is by upgrading from a ModelReport to a QueryReportView.  This is a bit similar to moving from a Django ModelForm to a Form, only with a little more magic.  You actually define each column that you want to include in the report, but the columns can get their data from any attribute, method, or even a chain of related models, all coming from a QuerySet that you provide.  We'll dig into more advanced configurations later, but for now, let's see how we would add info from a related model::q�hh�h h#h%hBh'}q�(h)]h*]h+]h,]h.]uh0K~h1hh]q�h:XX  Now, let's say that we wanted to add the industry column from the Company field.  The quickest way to do this is by upgrading from a ModelReport to a QueryReportView.  This is a bit similar to moving from a Django ModelForm to a Form, only with a little more magic.  You actually define each column that you want to include in the report, but the columns can get their data from any attribute, method, or even a chain of related models, all coming from a QuerySet that you provide.  We'll dig into more advanced configurations later, but for now, let's see how we would add info from a related model:qㅁq�}q�(hXX  Now, let's say that we wanted to add the industry column from the Company field.  The quickest way to do this is by upgrading from a ModelReport to a QueryReportView.  This is a bit similar to moving from a Django ModelForm to a Form, only with a little more magic.  You actually define each column that you want to include in the report, but the columns can get their data from any attribute, method, or even a chain of related models, all coming from a QuerySet that you provide.  We'll dig into more advanced configurations later, but for now, let's see how we would add info from a related model:hh�ubaubh^)�q�}q�(hX  class ContactsActiveColumns(QueryReport):
    first_name = columns.Text()
    middle_name = columns.Text()
    last_name = columns.Text()

    industry = columns.Text('company__get_industry_display')

    class Meta:
    queryset = Contact.objects.filter(active=True)hh�h h#h%hah'}q�(hchdh,]h+]h)]h*]h.]uh0K�h1hh]q�h:X  class ContactsActiveColumns(QueryReport):
    first_name = columns.Text()
    middle_name = columns.Text()
    last_name = columns.Text()

    industry = columns.Text('company__get_industry_display')

    class Meta:
    queryset = Contact.objects.filter(active=True)qꅁq�}q�(hU hh�ubaubh>)�q�}q�(hX�  The first three columns are pretty basic.  The Text column is the most basic column.  It grabs its data from the correponding column from an object in the ``QuerySet``.  If the column name is different or you want to reference a related column, you can include an argument, as with the industry column in the example above.  The format for referencing related columns should be familiar to Django folks.  The string is very similar to a python dot path, where '__' replaces the dot.  So, ``industry = columns.Text('company__get_industry_display')`` tells the column to get it's data from ``obj.company.get_industry_display``, where ``obj`` is an object in the ``QuerySet``.q�hh�h h#h%hBh'}q�(h)]h*]h+]h,]h.]uh0K�h1hh]q�(h:X�   The first three columns are pretty basic.  The Text column is the most basic column.  It grabs its data from the correponding column from an object in the q�q�}q�(hX�   The first three columns are pretty basic.  The Text column is the most basic column.  It grabs its data from the correponding column from an object in the hh�ubh�)�q�}q�(hX   ``QuerySet``h'}q�(h)]h*]h+]h,]h.]uhh�h]q�h:X   QuerySetq���q�}q�(hU hh�ubah%h�ubh:XA  .  If the column name is different or you want to reference a related column, you can include an argument, as with the industry column in the example above.  The format for referencing related columns should be familiar to Django folks.  The string is very similar to a python dot path, where '__' replaces the dot.  So, q���q�}q�(hXA  .  If the column name is different or you want to reference a related column, you can include an argument, as with the industry column in the example above.  The format for referencing related columns should be familiar to Django folks.  The string is very similar to a python dot path, where '__' replaces the dot.  So, hh�ubh�)�q�}r   (hX<   ``industry = columns.Text('company__get_industry_display')``h'}r  (h)]h*]h+]h,]h.]uhh�h]r  h:X8   industry = columns.Text('company__get_industry_display')r  ��r  }r  (hU hh�ubah%h�ubh:X(    tells the column to get it's data from r  ��r  }r  (hX(    tells the column to get it's data from hh�ubh�)�r	  }r
  (hX$   ``obj.company.get_industry_display``h'}r  (h)]h*]h+]h,]h.]uhh�h]r  h:X    obj.company.get_industry_displayr  ��r  }r  (hU hj	  ubah%h�ubh:X   , where r  ��r  }r  (hX   , where hh�ubh�)�r  }r  (hX   ``obj``h'}r  (h)]h*]h+]h,]h.]uhh�h]r  h:X   objr  ��r  }r  (hU hj  ubah%h�ubh:X    is an object in the r  ��r  }r  (hX    is an object in the hh�ubh�)�r  }r  (hX   ``QuerySet``h'}r  (h)]h*]h+]h,]h.]uhh�h]r   h:X   QuerySetr!  ��r"  }r#  (hU hj  ubah%h�ubh:X   .��r$  }r%  (hX   .hh�ubeubh>)�r&  }r'  (hX�   What if you want to include the results of a view method in your report.  That's easy too.  Just define a new method on the view and include the "path" to the attribute/method in your column definition::r(  hh�h h#h%hBh'}r)  (h)]h*]h+]h,]h.]uh0K�h1hh]r*  h:X�   What if you want to include the results of a view method in your report.  That's easy too.  Just define a new method on the view and include the "path" to the attribute/method in your column definition:r+  ��r,  }r-  (hX�   What if you want to include the results of a view method in your report.  That's easy too.  Just define a new method on the view and include the "path" to the attribute/method in your column definition:hj&  ubaubh^)�r.  }r/  (hX�  class ContactsActiveViewMethod(QueryReport):
    name = columns.Text('get_full_name')
    industry = columns.Text('get_industry')

    def get_industry(self, contact):
        """
        The first argument is passed the current object from the QuerySet.  you can return anything you need.
        """
        return contact.company.get_industry_display()

        class Meta:
        queryset = Contact.objects.filter(active=True)hh�h h#h%hah'}r0  (hchdh,]h+]h)]h*]h.]uh0K�h1hh]r1  h:X�  class ContactsActiveViewMethod(QueryReport):
    name = columns.Text('get_full_name')
    industry = columns.Text('get_industry')

    def get_industry(self, contact):
        """
        The first argument is passed the current object from the QuerySet.  you can return anything you need.
        """
        return contact.company.get_industry_display()

        class Meta:
        queryset = Contact.objects.filter(active=True)r2  ��r3  }r4  (hU hj.  ubaubh>)�r5  }r6  (hX7   Read more about :doc:`Attribute Resolution <resolver>`.r7  hh�h h#h%hBh'}r8  (h)]h*]h+]h,]h.]uh0K�h1hh]r9  (h:X   Read more about r:  ��r;  }r<  (hX   Read more about hj5  ubcsphinx.addnodes
pending_xref
r=  )�r>  }r?  (hX&   :doc:`Attribute Resolution <resolver>`r@  hj5  h h#h%Upending_xrefrA  h'}rB  (UreftypeX   docrC  UrefwarnrD  �U	reftargetrE  X   resolverU	refdomainU h,]h+]Urefexplicit�h)]h*]h.]UrefdocrF  UintroductionrG  uh0K�h]rH  h�)�rI  }rJ  (hj@  h'}rK  (h)]h*]rL  (UxrefrM  jC  eh+]h,]h.]uhj>  h]rN  h:X   Attribute ResolutionrO  ��rP  }rQ  (hU hjI  ubah%h�ubaubh:X   .��rR  }rS  (hX   .hj5  ubeubh>)�rT  }rU  (hXm  So now that you can control the columns that end up in your report, let's look at how you can control the display of individual columns.  There are a few built-in columns that you can use to change the display.  For example, you might want a column's data to display as a link on each row.  Let's say that we'd like to link to the admin view of the contact object::rV  hh�h h#h%hBh'}rW  (h)]h*]h+]h,]h.]uh0K�h1hh]rX  h:Xl  So now that you can control the columns that end up in your report, let's look at how you can control the display of individual columns.  There are a few built-in columns that you can use to change the display.  For example, you might want a column's data to display as a link on each row.  Let's say that we'd like to link to the admin view of the contact object:rY  ��rZ  }r[  (hXl  So now that you can control the columns that end up in your report, let's look at how you can control the display of individual columns.  There are a few built-in columns that you can use to change the display.  For example, you might want a column's data to display as a link on each row.  Let's say that we'd like to link to the admin view of the contact object:hjT  ubaubh^)�r\  }r]  (hX  class ActiveContactReport(QueryReportView):
    name = columns.AdminLink('get_full_name', reverse_path='contacts_contact_change', reverse_args=('id',))
    industry = columns.Text('company__industry')

    class Meta:
        queryset = Contact.objects.filter(active=True)hh�h h#h%hah'}r^  (hchdh,]h+]h)]h*]h.]uh0K�h1hh]r_  h:X  class ActiveContactReport(QueryReportView):
    name = columns.AdminLink('get_full_name', reverse_path='contacts_contact_change', reverse_args=('id',))
    industry = columns.Text('company__industry')

    class Meta:
        queryset = Contact.objects.filter(active=True)r`  ��ra  }rb  (hU hj\  ubaubh>)�rc  }rd  (hXt  Whoah!!  There's a bit of magic going on here.  If you're used to reversing Django's admin URLs, you probably already know what's going on.  If not, you might want to take a look at `Django's admin reversing docs <https://docs.djangoproject.com/en/dev/ref/contrib/admin/#reversing-admin-urls>`_.  The second and third attributes to the AdminLink class are the reverse path and args for the Django ``reverse`` command.  So, the ``name`` definition above results in calling ``reverse('admin:contacts_contact_change', args=(obj.id,))`` for each object in the QuerySet, and thus providing the link to the change view of that object.re  hh�h h#h%hBh'}rf  (h)]h*]h+]h,]h.]uh0K�h1hh]rg  (h:X�   Whoah!!  There's a bit of magic going on here.  If you're used to reversing Django's admin URLs, you probably already know what's going on.  If not, you might want to take a look at rh  ��ri  }rj  (hX�   Whoah!!  There's a bit of magic going on here.  If you're used to reversing Django's admin URLs, you probably already know what's going on.  If not, you might want to take a look at hjc  ubcdocutils.nodes
reference
rk  )�rl  }rm  (hXp   `Django's admin reversing docs <https://docs.djangoproject.com/en/dev/ref/contrib/admin/#reversing-admin-urls>`_h'}rn  (UnameX   Django's admin reversing docsUrefuriro  XM   https://docs.djangoproject.com/en/dev/ref/contrib/admin/#reversing-admin-urlsrp  h,]h+]h)]h*]h.]uhjc  h]rq  h:X   Django's admin reversing docsrr  ��rs  }rt  (hU hjl  ubah%U	referenceru  ubcdocutils.nodes
target
rv  )�rw  }rx  (hXP    <https://docs.djangoproject.com/en/dev/ref/contrib/admin/#reversing-admin-urls>U
referencedry  Khjc  h%Utargetrz  h'}r{  (Urefurijp  h,]r|  hah+]h)]h*]h.]r}  hauh]ubh:Xg   .  The second and third attributes to the AdminLink class are the reverse path and args for the Django r~  ��r  }r�  (hXg   .  The second and third attributes to the AdminLink class are the reverse path and args for the Django hjc  ubh�)�r�  }r�  (hX   ``reverse``h'}r�  (h)]h*]h+]h,]h.]uhjc  h]r�  h:X   reverser�  ��r�  }r�  (hU hj�  ubah%h�ubh:X    command.  So, the r�  ��r�  }r�  (hX    command.  So, the hjc  ubh�)�r�  }r�  (hX   ``name``h'}r�  (h)]h*]h+]h,]h.]uhjc  h]r�  h:X   namer�  ��r�  }r�  (hU hj�  ubah%h�ubh:X%    definition above results in calling r�  ��r�  }r�  (hX%    definition above results in calling hjc  ubh�)�r�  }r�  (hX<   ``reverse('admin:contacts_contact_change', args=(obj.id,))``h'}r�  (h)]h*]h+]h,]h.]uhjc  h]r�  h:X8   reverse('admin:contacts_contact_change', args=(obj.id,))r�  ��r�  }r�  (hU hj�  ubah%h�ubh:X`    for each object in the QuerySet, and thus providing the link to the change view of that object.r�  ��r�  }r�  (hX`    for each object in the QuerySet, and thus providing the link to the change view of that object.hjc  ubeubh>)�r�  }r�  (hX�  Pretty cool, yeah?  What if you just want to provide a link using ``reverse``?  Well, check out the ``ReverseLink`` class.  Actually, ``AdminLink`` is just a simple extension of ``ReverseLink``.  None of this suits you?  Fine, make your own!  Extend the ``Link`` class or even the base ``Text`` or ``Column`` classes to do whatever you want.  In fact, the goal is to provide a lot of basic types of column widgets that most people can just use, but allow you to easily extend any of them for your own use.r�  hh�h h#h%hBh'}r�  (h)]h*]h+]h,]h.]uh0K�h1hh]r�  (h:XB   Pretty cool, yeah?  What if you just want to provide a link using r�  ��r�  }r�  (hXB   Pretty cool, yeah?  What if you just want to provide a link using hj�  ubh�)�r�  }r�  (hX   ``reverse``h'}r�  (h)]h*]h+]h,]h.]uhj�  h]r�  h:X   reverser�  ��r�  }r�  (hU hj�  ubah%h�ubh:X   ?  Well, check out the r�  ��r�  }r�  (hX   ?  Well, check out the hj�  ubh�)�r�  }r�  (hX   ``ReverseLink``h'}r�  (h)]h*]h+]h,]h.]uhj�  h]r�  h:X   ReverseLinkr�  ��r�  }r�  (hU hj�  ubah%h�ubh:X    class.  Actually, r�  ��r�  }r�  (hX    class.  Actually, hj�  ubh�)�r�  }r�  (hX   ``AdminLink``h'}r�  (h)]h*]h+]h,]h.]uhj�  h]r�  h:X	   AdminLinkr�  ��r�  }r�  (hU hj�  ubah%h�ubh:X    is just a simple extension of r�  ��r�  }r�  (hX    is just a simple extension of hj�  ubh�)�r�  }r�  (hX   ``ReverseLink``h'}r�  (h)]h*]h+]h,]h.]uhj�  h]r�  h:X   ReverseLinkr�  ��r�  }r�  (hU hj�  ubah%h�ubh:X=   .  None of this suits you?  Fine, make your own!  Extend the r�  ��r�  }r�  (hX=   .  None of this suits you?  Fine, make your own!  Extend the hj�  ubh�)�r�  }r�  (hX   ``Link``h'}r�  (h)]h*]h+]h,]h.]uhj�  h]r�  h:X   Linkr�  ��r�  }r�  (hU hj�  ubah%h�ubh:X    class or even the base r�  ��r�  }r�  (hX    class or even the base hj�  ubh�)�r�  }r�  (hX   ``Text``h'}r�  (h)]h*]h+]h,]h.]uhj�  h]r�  h:X   Textr�  ��r�  }r�  (hU hj�  ubah%h�ubh:X    or r�  ��r�  }r�  (hX    or hj�  ubh�)�r�  }r�  (hX
   ``Column``h'}r�  (h)]h*]h+]h,]h.]uhj�  h]r�  h:X   Columnr�  ��r�  }r�  (hU hj�  ubah%h�ubh:X�    classes to do whatever you want.  In fact, the goal is to provide a lot of basic types of column widgets that most people can just use, but allow you to easily extend any of them for your own use.r�  ��r�  }r�  (hX�    classes to do whatever you want.  In fact, the goal is to provide a lot of basic types of column widgets that most people can just use, but allow you to easily extend any of them for your own use.hj�  ubeubh>)�r�  }r�  (hX*   Learn more about :doc:`Columns <columns>`.r�  hh�h h#h%hBh'}r�  (h)]h*]h+]h,]h.]uh0K�h1hh]r�  (h:X   Learn more about r�  ��r�  }r�  (hX   Learn more about hj�  ubj=  )�r�  }r�  (hX   :doc:`Columns <columns>`r�  hj�  h h#h%jA  h'}r�  (UreftypeX   docr�  jD  �jE  X   columnsU	refdomainU h,]h+]Urefexplicit�h)]h*]h.]jF  jG  uh0K�h]r�  h�)�r�  }r�  (hj�  h'}r�  (h)]h*]r�  (jM  j�  eh+]h,]h.]uhj�  h]r�  h:X   Columnsr   ��r  }r  (hU hj�  ubah%h�ubaubh:X   .��r  }r  (hX   .hj�  ubeubeubh)�r  }r  (hU hhh h#h%h&h'}r  (h)]h*]h+]h,]r  hah.]r	  hauh0K�h1hh]r
  (h3)�r  }r  (hX   Filtersr  hj  h h#h%h7h'}r  (h)]h*]h+]h,]h.]uh0K�h1hh]r  h:X   Filtersr  ��r  }r  (hj  hj  ubaubh>)�r  }r  (hX�  Filters are a core concept to the project and where a lot of the power and flexibility comes from.  Every column can accept a list of filters that can be applied to the QuerySet for that column.  Some filters are so common that they are included by the ``Column`` class.  For example, a date range filter is so common that there is a ``Date`` column that includes the ``DateRange`` filter by default.  Of course, this is easy to override.r  hj  h h#h%hBh'}r  (h)]h*]h+]h,]h.]uh0K�h1hh]r  (h:X�   Filters are a core concept to the project and where a lot of the power and flexibility comes from.  Every column can accept a list of filters that can be applied to the QuerySet for that column.  Some filters are so common that they are included by the r  ��r  }r  (hX�   Filters are a core concept to the project and where a lot of the power and flexibility comes from.  Every column can accept a list of filters that can be applied to the QuerySet for that column.  Some filters are so common that they are included by the hj  ubh�)�r  }r  (hX
   ``Column``h'}r  (h)]h*]h+]h,]h.]uhj  h]r  h:X   Columnr  ��r   }r!  (hU hj  ubah%h�ubh:XG    class.  For example, a date range filter is so common that there is a r"  ��r#  }r$  (hXG    class.  For example, a date range filter is so common that there is a hj  ubh�)�r%  }r&  (hX   ``Date``h'}r'  (h)]h*]h+]h,]h.]uhj  h]r(  h:X   Dater)  ��r*  }r+  (hU hj%  ubah%h�ubh:X    column that includes the r,  ��r-  }r.  (hX    column that includes the hj  ubh�)�r/  }r0  (hX   ``DateRange``h'}r1  (h)]h*]h+]h,]h.]uhj  h]r2  h:X	   DateRanger3  ��r4  }r5  (hU hj/  ubah%h�ubh:X9    filter by default.  Of course, this is easy to override.r6  ��r7  }r8  (hX9    filter by default.  Of course, this is easy to override.hj  ubeubh>)�r9  }r:  (hXd   Let's add date columns to our report to see how the ``DateRange`` filter is implemented by default::r;  hj  h h#h%hBh'}r<  (h)]h*]h+]h,]h.]uh0K�h1hh]r=  (h:X4   Let's add date columns to our report to see how the r>  ��r?  }r@  (hX4   Let's add date columns to our report to see how the hj9  ubh�)�rA  }rB  (hX   ``DateRange``h'}rC  (h)]h*]h+]h,]h.]uhj9  h]rD  h:X	   DateRangerE  ��rF  }rG  (hU hjA  ubah%h�ubh:X"    filter is implemented by default:rH  ��rI  }rJ  (hX"    filter is implemented by default:hj9  ubeubh^)�rK  }rL  (hXL  class ActiveContactReport(QueryReportView):
    first_name = columns.Text()
    middle_name = columns.Text()
    last_name = columns.Text()

    created = columns.Date()
    updated = columns.Date()

    industry = columns.Text('company__get_industry_display')

    class Meta:
        queryset = Contact.objects.filter(active=True)hj  h h#h%hah'}rM  (hchdh,]h+]h)]h*]h.]uh0K�h1hh]rN  h:XL  class ActiveContactReport(QueryReportView):
    first_name = columns.Text()
    middle_name = columns.Text()
    last_name = columns.Text()

    created = columns.Date()
    updated = columns.Date()

    industry = columns.Text('company__get_industry_display')

    class Meta:
        queryset = Contact.objects.filter(active=True)rO  ��rP  }rQ  (hU hjK  ubaubh>)�rR  }rS  (hX�   Both the ``created`` and ``updated`` columns get a DateRange filter by default.  You may have also noticed by now that the ``Text`` column includes the ``Search`` filter by default, as long as the column can be queried via the Django ORM.rT  hj  h h#h%hBh'}rU  (h)]h*]h+]h,]h.]uh0K�h1hh]rV  (h:X	   Both the rW  ��rX  }rY  (hX	   Both the hjR  ubh�)�rZ  }r[  (hX   ``created``h'}r\  (h)]h*]h+]h,]h.]uhjR  h]r]  h:X   createdr^  ��r_  }r`  (hU hjZ  ubah%h�ubh:X    and ra  ��rb  }rc  (hX    and hjR  ubh�)�rd  }re  (hX   ``updated``h'}rf  (h)]h*]h+]h,]h.]uhjR  h]rg  h:X   updatedrh  ��ri  }rj  (hU hjd  ubah%h�ubh:XW    columns get a DateRange filter by default.  You may have also noticed by now that the rk  ��rl  }rm  (hXW    columns get a DateRange filter by default.  You may have also noticed by now that the hjR  ubh�)�rn  }ro  (hX   ``Text``h'}rp  (h)]h*]h+]h,]h.]uhjR  h]rq  h:X   Textrr  ��rs  }rt  (hU hjn  ubah%h�ubh:X    column includes the ru  ��rv  }rw  (hX    column includes the hjR  ubh�)�rx  }ry  (hX
   ``Search``h'}rz  (h)]h*]h+]h,]h.]uhjR  h]r{  h:X   Searchr|  ��r}  }r~  (hU hjx  ubah%h�ubh:XL    filter by default, as long as the column can be queried via the Django ORM.r  ��r�  }r�  (hXL    filter by default, as long as the column can be queried via the Django ORM.hjR  ubeubh>)�r�  }r�  (hX�   Now, what if the attribute that displays the data is not the same as the column that filters the data?  Well, that's not a big deal.  You just manually define the filter, and provide it with the column(s) to use for filtering::r�  hj  h h#h%hBh'}r�  (h)]h*]h+]h,]h.]uh0K�h1hh]r�  h:X�   Now, what if the attribute that displays the data is not the same as the column that filters the data?  Well, that's not a big deal.  You just manually define the filter, and provide it with the column(s) to use for filtering:r�  ��r�  }r�  (hX�   Now, what if the attribute that displays the data is not the same as the column that filters the data?  Well, that's not a big deal.  You just manually define the filter, and provide it with the column(s) to use for filtering:hj�  ubaubh^)�r�  }r�  (hX|  from canary import filters

class ActiveContactReport(QueryReportView):
    full_name = columns.Text('get_full_name', filters=[
        filters.Search(columns=('first_name, middle_name, last_name')])

    created = columns.Date()
    updated = columns.Date()

    industry = columns.Text('company__industry')

    class Meta:
        queryset = Contact.objects.filter(active=True)hj  h h#h%hah'}r�  (hchdh,]h+]h)]h*]h.]uh0K�h1hh]r�  h:X|  from canary import filters

class ActiveContactReport(QueryReportView):
    full_name = columns.Text('get_full_name', filters=[
        filters.Search(columns=('first_name, middle_name, last_name')])

    created = columns.Date()
    updated = columns.Date()

    industry = columns.Text('company__industry')

    class Meta:
        queryset = Contact.objects.filter(active=True)r�  ��r�  }r�  (hU hj�  ubaubcdocutils.nodes
note
r�  )�r�  }r�  (hX�   The Search filter may not have worked on the earlier examples for this very reason.  For example, the ContactsActiveViewMethod in the demo project is updated from the example to specify the correct columns to search.r�  hj  h h#h%Unoter�  h'}r�  (h)]h*]h+]h,]h.]uh0Nh1hh]r�  h>)�r�  }r�  (hj�  hj�  h h#h%hBh'}r�  (h)]h*]h+]h,]h.]uh0K�h]r�  h:X�   The Search filter may not have worked on the earlier examples for this very reason.  For example, the ContactsActiveViewMethod in the demo project is updated from the example to specify the correct columns to search.r�  ��r�  }r�  (hj�  hj�  ubaubaubh>)�r�  }r�  (hXw   Filters are where you can add a lot of custom functionality to your reports.  Learn more about :doc:`Filters <filters>`r�  hj  h h#h%hBh'}r�  (h)]h*]h+]h,]h.]uh0K�h1hh]r�  (h:X_   Filters are where you can add a lot of custom functionality to your reports.  Learn more about r�  ��r�  }r�  (hX_   Filters are where you can add a lot of custom functionality to your reports.  Learn more about hj�  ubj=  )�r�  }r�  (hX   :doc:`Filters <filters>`r�  hj�  h h#h%jA  h'}r�  (UreftypeX   docr�  jD  �jE  X   filtersU	refdomainU h,]h+]Urefexplicit�h)]h*]h.]jF  jG  uh0K�h]r�  h�)�r�  }r�  (hj�  h'}r�  (h)]h*]r�  (jM  j�  eh+]h,]h.]uhj�  h]r�  h:X   Filtersr�  ��r�  }r�  (hU hj�  ubah%h�ubaubeubeubeubahU Utransformerr�  NUfootnote_refsr�  }r�  Urefnamesr�  }r�  Usymbol_footnotesr�  ]r�  Uautofootnote_refsr�  ]r�  Usymbol_footnote_refsr�  ]r�  U	citationsr�  ]r�  h1hUcurrent_liner�  NUtransform_messagesr�  ]r�  Ureporterr�  NUid_startr�  KUautofootnotesr�  ]r�  Ucitation_refsr�  }r�  Uindirect_targetsr�  ]r�  Usettingsr�  (cdocutils.frontend
Values
r�  or�  }r�  (Ufootnote_backlinksr�  KUrecord_dependenciesr�  NUrfc_base_urlr�  Uhttp://tools.ietf.org/html/r�  U	tracebackr�  �Upep_referencesr�  NUstrip_commentsr�  NUtoc_backlinksr�  Uentryr�  Ulanguage_coder�  Uenr�  U	datestampr�  NUreport_levelr�  KU_destinationr�  NU
halt_levelr�  KUstrip_classesr�  Nh7NUerror_encoding_error_handlerr�  Ubackslashreplacer�  Udebugr�  NUembed_stylesheetr�  �Uoutput_encoding_error_handlerr�  Ustrictr�  Usectnum_xformr�  KUdump_transformsr�  NUdocinfo_xformr�  KUwarning_streamr�  NUpep_file_url_templater�  Upep-%04dr�  Uexit_status_levelr�  KUconfigr�  NUstrict_visitorr�  NUcloak_email_addressesr�  �Utrim_footnote_reference_spacer�  �Uenvr�  NUdump_pseudo_xmlr�  NUexpose_internalsr�  NUsectsubtitle_xformr�  �Usource_linkr�  NUrfc_referencesr�  NUoutput_encodingr�  Uutf-8r�  U
source_urlr�  NUinput_encodingr�  U	utf-8-sigr�  U_disable_configr�  NU	id_prefixr�  U U	tab_widthr�  KUerror_encodingr   UUTF-8r  U_sourcer  U3/Users/bdavis/D/django-canary/docs/introduction.rstr  Ugettext_compactr  �U	generatorr  NUdump_internalsr  NUpep_base_urlr  Uhttp://www.python.org/dev/peps/r  Usyntax_highlightr	  Ushortr
  Uinput_encoding_error_handlerr  j�  Uauto_id_prefixr  Uidr  Udoctitle_xformr  �Ustrip_elements_with_classesr  NU_config_filesr  ]Ufile_insertion_enabledr  KUraw_enabledr  KUdump_settingsr  NubUsymbol_footnote_startr  K Uidsr  }r  (hhHhhhjw  hj  hh�uUsubstitution_namesr  }r  h%h1h'}r  (h)]h,]h+]Usourceh#h*]h.]uU	footnotesr  ]r  Urefidsr  }r  ub.