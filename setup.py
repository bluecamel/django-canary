#!/usr/bin/env python
# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

setup(
    name='django-canary',
    version='0.1',
    description='Reporting for Django',
    author='Branton K. Davis',
    author_email='bluecamel@gmail.com',
    long_description=open('README.md', 'r').read(),
    url='http://bitbucket.org/bluecamel/django-canary/',
    install_requires=[
        'django>=1.3.1',
    ],
    packages=find_packages(),
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Utilities'
    ],
)
